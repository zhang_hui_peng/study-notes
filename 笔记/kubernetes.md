

#### 运行一个replicationcontroller：

```shell
kubectl run kubia --image=kubia --port=8080 --image-pull-policy=IfNotPresent
```

#### 查看pod：

```shell
kubectl get pods
```

#### 列出pod时显示pod IP和pod的节点：

```shell
kubectl get pods -o wide
```

#### 查看pod的其他细节：

```shell
kubectl describe pod kubia-5bz82
```

#### **一个pod的所有容器都运行在同一个节点。**

#### 查看pod的yaml文件是如何定义的：

```shell
kubectl get pod kubia-5bz82 -o yaml
```

#### 查看pod的日志：

```shell
kubectl logs kubia-manual
```

#### 获取多容器pod的日志时指定容器名称：

```shell
kubectl logs kubia-manual -c kubia
```

#### 重新生成token

```shell
kubeadm token create --print-join-command
```

#### 创建在配置文件中定义的对象

```shell
kubectl create -f nginx.yaml
```

#### 删除在两个配置文件中定义的对象

```shell
kubectl delete -f nginx.yaml -f redis.yaml
```

#### 通过覆盖实时配置来更新配置文件中定义的对象

```shell
kubectl replace -f nginx.yaml	
```

#### 使用`diff`子命令查看将要进行的更改

```shell
kubectl diff -f configs/
```

#### 递归处理

```shell
kubectl diff -R -f configs/
kubectl apply -R -f configs/
```

#### 筛选出 [`status.phase`](https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle/#pod-phase) 字段值为 `Running` 的所有 Pod

```shell
kubectl get pods --field-selector status.phase=Running
```

#### 监控k8s资源

```shell
kubectl get --watch deployment
```

#### 运行一个deployment

```shell
kubectl run kubia --image=192.168.4.72:5000/kubia:0.1 --replicas=3 --port=8080
```

#### 获取pod的label信息

```shell
kubectl get pods --show-labels
```

#### 给pod添加新的标签

```shell
kubectl label pods kubia-b4cdb6964-grgkl tie=front
```

#### 删除pod的标签

```shell
kubectl label pods kubia-b4cdb6964-grgkl tie-
```

#### 修改pod的label标签

```shell
kubectl label pods kubia-b4cdb6964-grgkl tie=fron --overwrite
```

#### 获取指定lable的pod

**相等型：**

```shell
kubectl get pods --show-labels -l tie=fron
```

**集合型：**

```shell
kubectl get pods --show-labels -l 'env in(test,dev)'
```

#### 查看deployment的历史记录

```shell
kubectl rollout history deployment kubia
```

#### 编辑deployment的配置信息

```shell
kubectl edit deployment kubia
```

#### 更新deployment当中的pod容器

```shell
kubectl set image deployment kubia kubia=kubia
```

#### 回滚deployment

```shell
kubectl rollout undo deployment kubia
```

