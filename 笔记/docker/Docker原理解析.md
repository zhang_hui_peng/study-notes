## Namespace命名空间

当前Linux一共实现了6种不同类型的namespace：

| Namespace类型     | 系统调用参数  | 内核版本 |
| ----------------- | ------------- | -------- |
| Mount Namespace   | CLONE_NEWNS   | 2.4.19   |
| UTS Namespace     | CLONE_NEWUTS  | 2.6.19   |
| IPC Namespace     | CLONE_NEWIPC  | 2.6.19   |
| PID Namespace     | CLONE_NEWPID  | 2.6.24   |
| Network Namespace | CLONE_NEWNET  | 2.6.29   |
| User Namespace    | CLONE_NEWUSER | 3.8      |

### Namespace的API

​	Namespace的API主要使用如下==3==个系统调用：

- **clone()**：创建新进程。根据系统调用参数来判断哪些类型的Namespace被创建，而且它们的子进程也会被包含到这些Namespace中。
- **unshare()**：将进程==移出==某个**Namespace**
- **setns()**：将进程==加入==到**Namespace**中

### UTS Namespace

​	UTS Namespace主要用来隔离==nodename==和==domainname==两个系统标识。在UTS Namespace里面，每个Namespace允许有自己的==hostname==。

```go
package main

import (
	"log"
	"os"
	"os/exec"
	"syscall"
)

func main() {
	cmd :=exec.Command("sh")
	cmd.SysProcAttr = &syscall.SysProcAttr{Cloneflags:syscall.CLONE_NEWUTS}
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	if err := cmd.Run();err != nil {
		log.Fatal(err)
	}
}
```

### IPC Namespace

​	IPC Namespace用来隔离==System V IPC==和==POSIX message queues==。每一个IPC Namespace都有自己的System V IPC和POSIX message queues。

```go
package main

import (
	"log"
	"os"
	"os/exec"
	"syscall"
)

func main() {
	command:= exec.Command("sh")
	command.SysProcAttr = &syscall.SysProcAttr{Cloneflags: syscall.CLONE_NEWUTS | syscall.CLONE_NEWIPC}
	command.Stdin = os.Stdin
	command.Stdout = os.Stdout
	command.Stderr = os.Stderr
	if err:= command.Run();err!= nil {
		log.Fatal(err)
	}
}
```

### PID Namespace

​	PID Namespace是用来隔离进程ID的。同样一个进程在不同的PID Namespace里可以拥有不同的PID。

```go
package main

import (
	"log"
	"os"
	"os/exec"
	"syscall"
)

func main() {
	command:= exec.Command("sh")
	command.SysProcAttr = &syscall.SysProcAttr{Cloneflags: syscall.CLONE_NEWUTS | syscall.CLONE_NEWIPC | syscall.CLONE_NEWPID}
	command.Stdin = os.Stdin
	command.Stdout = os.Stdout
	command.Stderr = os.Stderr
	if err:= command.Run();err!= nil {
		log.Fatal(err)
	}
}
```

### Mount Namespace

​	Mount Namespace用来隔离各个进程看到的挂载点视图。在不同Namespace的进程中，看到的文件系统层次是不一样的。在Mount Namespace中调用==mount()==和==umount()==仅仅只会影响当前Namespace内的文件系统，而对全局的文件系统没有影响。

```go
package main

import (
	"log"
	"os"
	"os/exec"
	"syscall"
)

func main() {
	command:= exec.Command("sh")
	command.SysProcAttr = &syscall.SysProcAttr{Cloneflags:syscall.CLONE_NEWUTS|syscall.CLONE_NEWPID|syscall.CLONE_NEWIPC|syscall.CLONE_NEWNS}
	command.Stdin= os.Stdin
	command.Stderr = os.Stderr
	command.Stdout = os.Stdout
	if err:=command.Run();err!= nil {
		log.Fatal(err)
	}
}
```

### User Namespace

​	User Namespace主要是隔离用户的用户组ID。也就是说，一个进程的==User ID==和==Group ID==在User Namespace内外可以是不同的。比较常用的是，在宿主机上以一个非root用户运行创建一个User Namespace，然后在User Namespace里面却映射成root用户。

```go
package main

import (
	"log"
	"os"
	"os/exec"
	"syscall"
)

func main() {
	command:= exec.Command("sh")
	command.SysProcAttr = &syscall.SysProcAttr{Cloneflags:syscall.CLONE_NEWUTS|syscall.CLONE_NEWPID|syscall.CLONE_NEWIPC|syscall.CLONE_NEWNS|syscall.CLONE_NEWUSER,
		UidMappings: []syscall.SysProcIDMap{
			{
				ContainerID: 0,
				HostID:      0,
				Size:        1,
			},
		},
		GidMappings: []syscall.SysProcIDMap{
			{
				ContainerID: 0,
				HostID:      0,
				Size:        1,
			},
		},
}

	command.Stdin= os.Stdin
	command.Stderr = os.Stderr
	command.Stdout = os.Stdout

	if err:=command.Run();err!= nil {
		log.Fatal(err)
	}
	os.Exit(-1)
}
```

### Network Namespace

​	Network Namespace是用来隔离==网络设备、IP地址端口==等网络栈的Namespace。Network Namespace可以让每个容器拥有自己独立的（虚拟的）网络设备，而且容器内的应用可以绑定到自己的端口，每个Namespace内的端口都不会互相冲突。在宿主机上搭建网桥后，就能很方便地实现容器之间的通信，而且不同容器上的应用可以使用相同的端口。

```go
package main

import (
	"log"
	"os"
	"os/exec"
	"syscall"
)

func main() {
	command:= exec.Command("sh")
	command.SysProcAttr = &syscall.SysProcAttr{Cloneflags:syscall.CLONE_NEWUTS|syscall.CLONE_NEWPID|syscall.CLONE_NEWIPC|syscall.CLONE_NEWNS|syscall.CLONE_NEWUSER|syscall.CLONE_NEWNET,
		UidMappings: []syscall.SysProcIDMap{
			{
				ContainerID: 0,
				HostID:      0,
				Size:        1,
			},
		},
		GidMappings: []syscall.SysProcIDMap{
			{
				ContainerID: 0,
				HostID:      0,
				Size:        1,
			},
		},
	}

	command.Stdin= os.Stdin
	command.Stderr = os.Stderr
	command.Stdout = os.Stdout

	if err:=command.Run();err!= nil {
		log.Fatal(err)
	}
	os.Exit(-1)
}
```

## Linux Cgroups

​	**Linux Cgroups( Control Groups)**提供了对一组进程及将来子进程的==资源限制、控制和统计==的能力，这些资源包括==CPU、内存、存储、网络==等。通过Cgroups，可以方便地限制某个进程的资源占用，并且可以实时地监控进程的监控和统计信息。

### Cgroups中的三个组件

- **cgroup**是对进程分组管理的一种机制，一个cgroup包含一组进程，并可以在这个cgroup上增加**Linux subsystem**的各种参数配置，将一组进程和一组subsystem的系统参数关联起来。
- **subsystem**是一组资源控制的模块，一般包含如下几项：
  - ==blkio==：设置对==块设备（比如硬盘）==输入输入的访问控制
  - ==cpu==：设置cgroup中==进程的CPU被调度的策略==
  - ==cpuacct==：可以统计cgroup中==进程的CPU占用==
  - ==cpuset==：在多核机器上设置cgroup中==进程可以使用的CPU和内存==（此处内存仅使用于NUMA架构）
  - ==devices==：控制cgroup中进程==对设备的访问==
  - ==freezer==：用于==挂起（suspend）和恢复（resume）==cgroup中的==进程==
  - ==memory==：用于控制cgroup中进程的==内存占用==
  - ==net-cls==：用于将cgroup中进程产生的网络包分类，以便**Linux**的**tc（traffic controller）**可以根据分类区分出来自某个cgroup的包并做限流或监控。
  - ==net_prio==：用于将cgroup中进程产生的==网络流量的优先级==
  - ==ns==：这个subsystem比较特殊，它的作用是使cgroup中的进程在新的Namespace中fork新进程（NEWNS）时，创建一个新的cgroup，这个cgroup包含新的Namespace中的进程。
- **hierarchy**：功能是把一组cgroup串成一个树状的结构，一个这样的树便是一个hierarchy，通过这种树状结构，Cgroups可以做到继承。比如，系统对一组定时的任务进程通过cgroup1限制了CPU的使用率，然后其中有一个定时dump日志的进程还需要限制磁盘IO，为了避免限制磁盘IO之后影响到其他进程，就可以创建cgroup2，使其继承于cgroup1并限制磁盘的IO，这样cgroup2便继承了cgroup1中对CPU使用率的限制，并增加了磁盘IO的限制而不影响到cgroup1中的其他进程

### 三个组件的相互关系

- 系统在创建了新的**hierarchy**之后，系统中所有的进程都会加入到这个**hierarchy**的**cgroup**根结点，这个cgroup根结点是hierarchy默认创建的。
- 一个**subsystem**只能附加到一个**hierarchy**上面
- 一个**hierarchy**可以附加多个**subsystem**
- 一个进程可以作为多个**cgroup**的成员，但是这些**cgroup**必须在不同的hierarchy中
- 一个进程**fork**出子进程时，子进程是和父进程在同一个cgroup中的，也可以根据需要将其移动到其他的cgroup中

### 创建并挂载一个hierarchy

```shell
mkdir cgroup-test #创建一个hierarchy挂载点
mount -t cgroup -o none,name=cgroup-test cgroup-test ./cgroup-test #挂载一个hierarchy
```

挂载后生成的文件：

- **cgroup.clone_children**：==cpuset==的subsystem会读取这个配置文件，如果这个值是1（默认是0），子cgroup才会继承父cgroup的配置
- **cgroup.procs**：是==树中当前节点cgroup中的进程组ID==，现在的位置是根结点，这个文件中会有现在系统中所有进程组的ID
- **notify_on_release**：会和**release_agent**一起使用。**notify_on_release**==标识当这个cgroup最后一个进程退出的时候是否执行了release_agent==；**release_agent**则是一个==路径==，通常用作==进程退出后自动清理掉不再使用的cgroup==。
- **tasks**：==标识该**cgroup**下面的进程ID==，**如果把一个进程ID写到tasks文件中，便会将相应的进程加入到这个cgroup中**

### docker是如何使用Cgroups的

​	docker会为每个容器在系统的hierarchy中创建cgroup

```shell
cd /sys/fs/cgroup/memory/docker/34f5784ac265b41253e2a0f6aa7a927afb5db6c15877d5e45
cat memory.limit_in_bytes #查看cgroup的内存限制
cat memory.usage_in_bytes #查看cgroup中的进程所使用的内存大小

```

用go语言实现通过cgroup限制容器的资源

```go
package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path"
	"strconv"
	"syscall"
)

const cgroupMemoryHierarchyMount = "/sys/fs/cgroup/memory"
func main() {
	if os.Args[0]== "/proc/self/exe" {
		fmt.Printf("current pid %d",syscall.Getpid())
		fmt.Println()
		cmd := exec.Command("sh","-c", `stress --vm-bytes 200m --vm-keep -m 1`)
		cmd.Stdin = os.Stdin
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		if err:= cmd.Run();err!= nil {
			fmt.Println(err)
			os.Exit(1)
		}
	}
	cmd := exec.Command("/proc/self/exe")
	cmd.SysProcAttr = &syscall.SysProcAttr{
		Cloneflags: syscall.CLONE_NEWUTS | syscall.CLONE_NEWPID | syscall.CLONE_NEWNS,
	}
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	if err:= cmd.Start();err!= nil {
		fmt.Println("ERROR",err)
		os.Exit(1)
	} else{
		//得到fork出来进程映射在外部命名空间的pid
		fmt.Printf("%v", cmd.Process.Pid)

		//在系统默认挂载了memory subsystem的Hierarchy上创建cgroup
		os.Mkdir(path.Join(cgroupMemoryHierarchyMount,"testmemorylimit"), 0755)
		//将容器进程加入到这个cgroup中
		ioutil.WriteFile(path.Join(cgroupMemoryHierarchyMount,"testmemorylimit","tasks"),[]byte(strconv.Itoa(cmd.Process.Pid)), 0644)

		//限制cgroup进程使用
		ioutil.WriteFile(path.Join(cgroupMemoryHierarchyMount,"testmemorylimit","memory.limit_in_bytes"),[]byte("100m"), 0644)


	}
	cmd.Process.Wait()
}
```

## Union File System

> ​	Union File System，简称**UnionFS**，是一种为Linux、FreeBSD和NetBSD操作系统设计的，把其他文件系统联合到一个联合挂载点的文件系统服务。它使用==branch==把不同文件系统的文件和目录“透明地”覆盖，形成一个单一一致的文件系统。这些branch或者是==read-only==的，或者是==read-write==的，所以当对这个虚拟后的联合文件系统进行==写操作==的时候，系统是真正的写到了一个新的文件中。看起来这个虚拟后的联合文件系统是可以对任何文件进行操作的，但是其实它并没有改变原来的文件，这是因为unionfs用到了一个重要的资源管理技术，叫==写时复制==。

### 写时复制

​	写时复制：也叫**隐式共享**，是一种对可修改资源实现高效复制的资源管理技术，它的思想是，如果一个资源是重复的，但没有人任何修改，这时并不需要立即创建一个新的资源，这个资源可以被新旧实例共享。创建新资源发生在第一次写操作，也就是对资源进行修改的时候。通过这种资源共享的方式，可以显著地减少未修改资源复制带来的消耗，但是也会在进行资源修改时增加小部分的开销。

### AUFS

​	AUFS：全称**Advanced Multi-Layered Unification Filesystem**，完全重写了早期的UnionFS 1.x，其主要目的是为了可靠性和性能，并且引入了一些新的功能，比如可写分支的负载均衡。