# Docker

## 核心概念

### Docker镜像

- Docker镜像类似于虚拟机镜像，可以将它理解为一个面向Docker引擎的只读模板，包含了文件系统。Docker镜像是由一层层只读的文件系统堆叠而成的。

#### 镜像相关命令

- 获取镜像：**docker pull NAME[:TAG]**,如果不显式指定TAG，则默认会使用latest标签，即下载最新版本的镜像。

- 查看本地所有镜像：**docker images**

- 为本地镜像添加新的标签：**docker tag NAME[:TAG] NAME[:TAG]**,虽然镜像的标签不一样，但是指向的是同一个镜像文件，标签在这里起到引用或者快捷方式的作用。

- 获取某个镜像的详细信息：**docker inspect 镜像ID或者名字**，命令返回的是一个json格式的消息，如果只要其中的某个内容，可以通过**-f**指定，列如：**docker inspect -f {{".Architecture"}} 镜像ID或者名字**

- 搜寻镜像：**docker search NAME**

- 删除镜像：**docker rmi NAME[:TAG]或者image ID**

- 创建镜像：1、根据已有容器创建镜像：**docker commit 容器名 NAME**

- 存出和导入镜像：存出：**docker save -o my_ubuntu_v3.tar runoob/ubuntu:v3**

  ​							   导入：**docker load < my_ubuntu_v3.tar**

- 上传镜像：**docker push 镜像名**，如果需要push到私有仓库，首先需要先对镜像进行**TAG**，添加上**IP**和端口。

### Docker容器

- 容器是镜像的一个运行实例，所不同的是，它带有额外的可写文件层。

#### 容器相关命令

- 新建并启动容器：**docker run -it ubuntu:14.04 /bin/bash**,其中**-t**选项让**Docker**分配一个伪终端并绑定到容器的标准输入上，**-i**则让容器的标准输入保持打开。
- 守护态运行：**docker run -d ...**,通过添加**-d**参数。
- 终止容器：**docker stop ...**
- 启动处于终止状态的容器：**docker start ...**
- 进入容器：**docker attach ...**或者**docker exec ...**,但是**attach**命令存在的问题是当多个窗口同时**attach**到同一个容器的时候，所有窗口都会同步显示。当某个窗口因命令阻塞，其他窗口也无法执行操作了。
- 删除容器：可以使用**docker rm ...**删除处于终止状态的容器。
- 导出容器：**docker export 容器ID >文件名.tar**
- 导入容器：**docker import 文件名 镜像名**，**load**和**import**导入镜像最大的区别在于，容器快照文件将丢弃所有的历史记录和元数据信息，而镜像存储文件将保存完整记录。

### 仓库

- 使用registry镜像创建私有仓库：**docker run -d -p 5000:5000 registry**，默认情况下会将仓库创建在容器的/tmp/registry目录下。可以通过**-v**参数来将镜像文件存放在本地的指定路径上。**docker run -d -p 5000:5000 -v /opt/data/registry:/tmp/registry registry**

### 数据管理

#### 数据卷

- 挂载一个主机目录作为数据卷：**docker run -d -p  --name web -v /src/webapp:/opt/webapp webapp**，上面的命令加载主机的/src/webapp目录到容器的/opt/webapp目录，docker挂载数据卷的默认权限是读写(**rw**),可以通过**ro**指定为只读：**docker run -d -p  --name web -v /src/webapp:/opt/webapp:ro webapp**
- 如果**-v**指定宿主机的目录是相对路径**test1**，则挂载的宿主机的目录为**/var/lib/docker/volumes/test1/_data**，如果**-v**只指定一个目录，则挂载的宿主机的目录为**/var/lib/docker/volumes/96256232eb74edb139d652746f0fe426e57fbacdf73376963e3acdb411b3d73a/_data**，为**/var/lib/docker/volumes/**底下随机生成的目录，当删除容器时，挂载的宿主机目录不会被删除。

#### 数据卷容器

- 如果用户需要在容器之间共享一些持续更新的数据，最简单的方式是使用数据卷容器。数据卷容器其实就是一个普通的容器，专门用它提供数据卷供其他容器挂载。
- 创建数据卷容器：**docker run -it -v /dbdata --name dbdata ubuntu**
- 其他容器可以使用**--volumes-from**来挂载**dbdata**容器中的数据卷：**docker run -it --volumes-from dbdata --name db1 ubuntu**，挂载数据卷的容器任何一方在该目录下写入，其他容器都可以看到。
- 删除数据卷：删除了挂载的容器，数据卷并不会被自动删除。如果要删除一个数据卷，必须在删除最后一个还挂载着它的容器时显式使用**docker rm -v **命令来指定。

##### 使用数据卷容器迁移数据

- 备份：**docker run --volumes-from dbdata -v ${pwd}:/backup --name worker ubuntu tar cvf /backup/backup.tar /dbdata**	命令解析如下，首先利用**ubuntu**镜像创建了一个容器**worker**，使用**--volumes-from dbdata**参数来让**worker**容器挂载**dbdata**容器的数据卷（即**dbdata**数据卷），使用**-v ${pwd}:/backup参数来挂载本地的当前目录到worker容器的/backup目录。worker**容器启动后，使用了**tar cvf /backup/backup.tar /dbdata**命令来将**/dbdata**下内容备份为容器内的**/backup/backup.tar**，即宿主机当前目录下的**backup.tar**
- 恢复：首先创建一个带有数据卷的容器 **dbdata2**，**docker run -v /dbdata --name dbdata2 ubuntu /bin/bash**，然后创建另一个新的容器，挂载**dbdata2**的容器，并使用解压命令解压备份文件到所挂载的容器卷中即可 **docker run --volumes-from dbdata2 -v ${pwd}:/backup busybox tar xvf /backup/backup.tar**

### Dockerfile

- 一般而言，**Dockerfile**分为四个部分：基础镜像信息、维护者信息、镜像操作指令和容器启动时执行指令

#### 指令

- **FROM**：格式为**FROM <image>**或**FROM<image>:<tag>**，第一条指令必须为**FROM**指令。
- **MAINTAINER**：格式为**MAINTAINER<name>**，指定维护者信息。
- **RUN**：格式为**RUN <command>**
- **CMD**
- **EXPOSE**
- **ENV**
- **ADD**
- **COPY**
- **ENTRYPOINT**
- **VOLUME**
- **USER**
- **WORKDIR**
- **ONBUILD**

#### 创建镜像

- **docker build -t 镜像名:标签 **默认读取当前目录下的**Dockerfile**文件，可以通过**-f**指定**Dockerfile**文件路径

> docker 创建service的时候需要先将镜像push到docker仓库
>
> /var/lib/docker/devicemapper/metadata目录
>
> 如果kubernetes的版本为1.8-1.11，docker版本必须为1.11.2-1.13.1和docker-ce版本为17.03.x
> 如果kubernetes的版本从1.12开始，docker版本必须为17.06/17.09/18.06

## 容器解析

![image-20200115101234636](C:\Users\Administrator\Desktop\study-notes\笔记\images\image-20200115101234636.png)

## 镜像解析

![image-20200115101508810](C:\Users\Administrator\Desktop\study-notes\笔记\images\image-20200115101508810.png)

**docker images**：

- **基于联合文件系统**
- **不同的层可以被其他镜像复用**
- **容器的可写层可以被做成镜像新的一层（commit）**

### 以overlay文件系统为例

![image-20200115101746841](C:\Users\Administrator\Desktop\study-notes\笔记\images\image-20200115101746841.png)

**mergedir：**整合了**lower**层和**upper**读写层显示出来的视图

**upper：**容器读写层

**workdir：**类似中间层，对**upper**层的写入，先写入到**workdir**，再移入**upper**层

**lower：**镜像层

#### 文件操作

- **读：如果upper层没有副本，数据都从lower读上来**
- **写：容器创建出来时，upper层是空的，只有对文件进行写操作时，才会从lower层拷贝文件上来，对副本进行操作。**
- **删：删操作不影响lower层，删除操作通过对文件进行标记，使文件无法显示。有两种方式，whiteout和设置目录的xattr “trusted.overlay.opaque”=y**

