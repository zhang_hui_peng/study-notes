# Go笔记

## 基础篇

1. 使用**var**关键字定义全局变量和那些跟初始化表达式类型不一致的局部变量所保留，或者用于后面才对变量赋值以及变量初始值不重要的情况。
2. 使用**:=**短声明进行局部变量的声明和初始化，**:=**是声明，**=**是赋值。
3. 使用**:=**短声明最少声明一个新变量，否则编译无法通过。
4. 如果一个变量声明为**var x int**，表达式**&x**获取一个指向整形变量的指针，类型为整形指针**(*int)**。如果值叫做**p**，**p**包含**x**的地址，表达式***p**获取变量的值。
5. 表达式**new(T)**创建一个未命名的**T**类型变量，初始化为**T**类型的零值，并返回其地址(地址类型为***T**)
6. 声明的作用域是声明在程序文本中出现的区域，它是一个编译时属性，变量的生命周期是变量在执行程序期间能被程序的其他部分所引用的起止时间，它是一个运行时属性。
7. GO是**强制类型转换**

## 内建变量类型

- **bool**
- **string**
- **(u)int、(u)int8、(u)int16、(u)int32、(u)int64、uintptr**：不规定长度的时候根据操作系统位数来决定长度
- **byte**：8位
- **rune**：字符型，**32位**
- **float32、float64**
- **complex64、complex128**

## 常量

go语言使用**const**来定义常量

```go
const (
	b = 1 << (10 % iota)
	kb
	_
	gb
	tb
	pb
)
```

## 条件语句

### switch

switch会自动break,除非使用fallthrough

### if

if的条件里可以赋值，条件里赋值的变量的作用域就在这个if语句里。if后面可以跟多个语句。

1. ```go
   if content, err := ioutils.ReadFile(filename); err == nil {
       fmt.Println(string(content))
   } else {
       fmt.Println("cannot read file", err)  
   }
   ```



## 内置数据结构

### 数组

```go
	//第一种定义数组形式
	var array1[5] int

	//第二种定义数组形式
	array2 := [5]int{1,2,3,4,5}

	//第三种定义数组形式
	array3 :=[...]int{1,2,3,4}

	//声明指针数组
	array4 :=[5]*int{0:new(int)}
	*array4[0] = 10

	//初始化多维数组
	var array5[4][2] int
	array5[0][1] = 10
	
	//使用数组字面量来声明并初始化一个二维整型数组
	array6 :=[4][2]int{{10,11},{20,21},{30,31},{40,41}}

	//声明并初始化外层数组中索引为1和3的元素
	array7 :=[4][2]int{1:{20,21},3:{40,41}}

	//声明并初始化外层数组和内层数组的单个元素
	array8 :=[4][2]int{1:{0:20},3:{0:40}}

	//使用索引为多维数组赋值
	var array9[2] int = array8[1]
	array10 := array8[1]
	var value int = array8[1][0]
```

### 切片

​	**切片是一个很小的对象，对底层数组进行了抽象，并提供相关的操作方法。切片有 3 个字段（指向底层数组的指针、切片访问的元素的个数（即长度）和切片允许增长 到的元素个数（即容量） ）的数据结构，这些数据结构包含 Go 语言需要操作底层数组的元数据。**

#### 切片的创建和初始化

1. 创建一个字符串切片，其长度和容量都为5

```go
slice1 := make([]string,5)
```

2. 创建一个整形切片，长度为3容量为5

```go
slice2 := make([]int,3,5)
```

3. 通过字面量来创建切片

```go
slice3 := []string{"Red","Blue","Green"}
```

4. 使用索引来创建切片

```go
slice4 := []string{99:""}
```

**如果在[]里面指定了一个值，那么创建的就是数组而不是切片**

#### 创建一个nil切片

```go
var slice5 []int
```

#### 使用切片

1. 创建一个切片，并改变值

```go
slice6 := []int{10,20,30,40}
slice6[0] = 25
```

2. 使用切片创建切片,两个切片共享同一个底层数组，如果一个切片修改了该底层数组的共享部分，另一个也能感知到。

```go
oldSlice := []int{10,20,30,40,50}
newSlice := oldSlice[1:3]
```

#### 切片增长

1. 通过使用append向切片增加元素（如果newSlice在底层数组当中还有额外容量可用，append将可用元素合并到切片的长度，并赋值。没有则创建一个新的底层数组，并复制现有的值到新数组中，再追加新的值。**新数组的容量为旧数组的两倍**）

```go
oldSlice := []int{10,20,30,40,50}
newSlice := oldSlice[1:3]
newSlice = append(newSlice, 8)
```

2. 使用3个索引创建切片（**如果在创建切片时设置切片的容量和长度一样，就可以强制让新切片的第一个 append 操作 创建新的底层数组，与原有的底层数组分离。**）

```go
source := []string{"Apple", "Orange", "Plum", "Banana", "Grape"}
sourceSlice :=source[2:3:4]
```

3. 使用**...**将一个切片追加到另一个切片

```go
s1 := []int{10,20}
s2 := []int{30,40}
fmt.Println(append(s1,s2...))
```

#### 迭代切片

1. 使用**for range**迭代切片

```go
sliceInt :=[]int{10,20,30}
for index,value :=range sliceInt {
	fmt.Printf("Index %d, Value %d\n",index,value)
}
```

**range创建了每个元素的副本，而不是直接返回对该元素的引用。**

2. 如果不需要索引值，可以使用占位符来忽略这个值

```go
slice :=[]int{10,20,30,40}
for _,value :=range slice {
	fmt.Println(value)
}
```

3. 使用传统的for循环对切片进行迭代

```go
slice :=[]int{10,20,30,40}
for index:=2;index<len(slice);index++{
	fmt.Println(slice[index])
}
```

4. 创建多维切片

```go
slice :=[][]int{{10},{100,200}}
slice[0] = append(slice[0],20)
fmt.Println(slice)
```

### 映射

1. 使用make声明映射

```go
dict := make(map[string]int)
dict["key"]= 123
```

**切片不能作为map的key**

2. 使用映射字面量声明一个空映射

```go
dict := map[int][]string{}
```

3. 通过声明映射创建一个nil映射（**nil映射 不能用于存储键值对，否则，会产生一个语言运行时错误**）

```go
var colors map[string]int
```

4. 使用range迭代映射

```go
colors := map[string]int{"red":1,"green":2}
for key,value := range colors {
	fmt.Println(key,value)
}
```

## 定义类型

1. 声明一个结构类型

```go
type user struct {
	name string
	email string
	ext int
	privileged bool
}
```

2. 使用结构类型声明变量，并初始化为零值

```go
var zhangsan user
```

3. 使用结构字面量来声明一个结构类型的变量

```go
lisa := user{
		name:  "Lisa",
		email: "lisa@email.com",
		ext:  123,
		privileged: true, 
}
```

4. 不使用字段名，创建结构类型的值

```go
lisa := user{"lisa","lisa@email.com",123,true}
```

5. 使用其他结构声明字段

```go
type admin struct {
	person user
	level string
}
```

6. 使用结构字面量来创建字段的值 

```go
fred := admin{
		person:user{
			name:  "Lisa",
			email: "lisa@email.com",
			ext:  123,
			privileged: true,
		},
		level: "super",
}
```

7. 基于已有类型，将其作为新类型的类型说明

```go
type Duration int64
```

## 定义方法

​	**方法能给用户定义的类型添加新的行为。方法实际上也是函数，只是在声明时，在关键字 func和方法名之间增加了一个参数**

1. 使用值接收者声明方法（**如果使用值接收者声明方法，调用时会使用这个值的一个副本来执行**）

```go
func (u user)notify()  {
	fmt.Printf("Send User Email to %s<%s>\n",u.name,u.email)
}
```

2. 使用指针接收者声明方法（**当调用使用指针接收者声明的方法时，这个方法会共享调用方法时接收者所指向的值**）

```go
func (u *user)changeEmail(email string)  {
	u.email = email
}
```

**重点：值接收者使用 值的副本来调用方法，而指针接受者使用实际值来调用方法**

## 定义接口

1. 定义一个接口，并定义一个类型实现该接口的notify方法。

```go
type notifier interface {
	notify()
}
type user struct {
	name  string
	email string
}
func (u *user) notify() {
	fmt.Printf("Sending user email to %s<%s>\n",
		u.name,
		u.email)
}
```

**如果使用指 针接收者来实现一个接口，那么只有指向那个类型的指针才能够实现对应的接口。如果使用值 接收者来实现一个接口，那么那个类型的值和指针都能够实现对应的接口**

## 实现多态

```go
package main

import (
	"fmt"
)

type notifier interface {
	notify()
}
type user struct {
	name string
	email string
}
type admin struct {
	name string
	email string
}

func (u *user)notify()  {
	fmt.Println(u.name,u.email)
}
func (a *admin) notify()  {
	fmt.Println(a.name,a.email)
}
func sendNotificaton(n notifier)  {
	n.notify()
}
func main() {
	zhangsan := user{"zhangsan","zhangsan@email.com"}
	lisa := admin{"lisa","lisa@email.com"}
	sendNotificaton(&zhangsan)
	sendNotificaton(&lisa)
}
```

## 嵌入类型

1. 声明一个嵌入类型（**要嵌入一个类型，只需要声明这个类型的名字就可以了，注意声明字段和嵌入类型在语法上的不同**）

```go
package main

import (
	"fmt"
)

type notifier interface {
	notify()
}
type user struct {
	name string
	email string
}
type admin struct {
	user
	level string
}
func (u *user)notify()  {
	fmt.Println(u.name,u.email)
}
func main() {
	ad := admin{
		user{
			"lisa",
			"lisa@email.com",
		},
		"admin",

	}
	//可以直接访问内部类型的方法
	ad.user.notify()
	//内部类型的方法也被提升到外部类型
	ad.notify()
}
```

**重点1：内部类型实现的接口会自动提升到外部类型。这意味着由于内部类型的 实现，外部类型也同样实现了这个接口。**

**重点2：如果外部类型 实现了notify方法，内部类型的实现就不会被提升。不过内部类型的值一直存在，因此还可以通过直接访问内部类型的值，来调用没有被提升的内部类型实现的方法**

## 标识符

​	**当一个标识符的名字以小写字母开头时，这个标识符就是未公开的，即包外的代码不可见。 如果一个标识符以大写字母开头，这个标识符就是公开的，即被包外的代码可见**

​	**在嵌入类型中，即便内部类型是未公开的，内部类型里声明的字段依旧是公开的。既然内部类型的标识符提升到了外部类型，这些公开的字段也可以通过外部类型的字段的值来访问**

## 并发

### Go语言并发模型

​	**Go 语言里的并发指的是能让某个函数独立于其他函数运行的能力。当一个函数创建为goroutine 时，Go 会将其视为一个独立的工作单元。这个单元会被调度到可用的逻辑处理器上执行。Go 语言 运行时的调度器是一个复杂的软件，能管理被创建的所有 goroutine 并为其分配执行时间。这个调度 器在操作系统之上，将操作系统的线程与语言运行时的逻辑处理器绑定，并在逻辑处理器上运行 goroutine。调度器在任何给定的时间，都会全面控制哪个goroutine 要在哪个逻辑处理器上运行。 **

​	**Go 语言的并发同步模型来自一个叫作通信顺序进程（Communicating Sequential Processes，CSP） 的范型（paradigm）。CSP 是一种消息传递模型，通过在goroutine 之间传递数据来传递消息，而不是 对数据进行加锁来实现同步访问。用于在 goroutine 之间同步和传递数据的关键数据类型叫作通道 （channel）。**



![image-20200303143110327](C:\Users\Administrator\Desktop\study-notes\笔记\images\image-20200303143110327.png)

​	**操作系统会在物理处理器上调度线程来运行，而 Go 语言的运行时会在逻辑处理器上调度 goroutine来运行。每个逻辑处理器都分别绑定到单个操作系统线程,在1.5版本上，Go语言的运行时默认为每个可用的物理处理器分配一个逻辑处理器。**

### goroutine的调度

![image-20200303143640535](C:\Users\Administrator\Desktop\study-notes\笔记\images\image-20200303143640535.png)

​		**在图 6-2 中，可以看到操作系统线程、逻辑处理器和本地运行队列之间的关系。如果创建一 个 goroutine 并准备运行，这个 goroutine 就会被放到调度器的全局运行队列中。之后，调度器就 将这些队列中的 goroutine 分配给一个逻辑处理器，并放到这个逻辑处理器对应的本地运行队列中。本地运行队列中的 goroutine 会一直等待直到自己被分配的逻辑处理器执行 **

### 竞争状态

1. 检测竞争状态工具

```go
go build -race example.go
```

### 锁住共享资源

#### 原子函数

​	**原子函数能够以很底层的加锁机制来同步访问整型变量和指针**

1. 使用atomic包中的AddInt64方法

```go
package main

import (
	"fmt"
	"runtime"
	"sync"
	"sync/atomic"
)
var(
	counter int64
	wait sync.WaitGroup
)
func main() {
	wait.Add(2)
	go incCounter(1)
	go incCounter(2)
	wait.Wait()
	fmt.Println(counter)

}
func incCounter(id int)  {
	defer wait.Done()

	for count := 0; count < 2; count++ {
		atomic.AddInt64(&counter,1)
		runtime.Gosched()
	}
}
```

2. 使用atomic包中的StoreInt64方法和LoadInt64方法

```go
package main

import (
	"fmt"
	"sync"
	"sync/atomic"
	"time"
)
var(
	shutdown int64
	WG sync.WaitGroup
)
func main() {
	WG.Add(2)
	go dowork("A")
	go dowork("B")
	time.Sleep(1*time.Second)
	atomic.StoreInt64(&shutdown,1)
	WG.Wait()
}
func dowork(name string) {
	defer WG.Done()

	for true {
		fmt.Printf("%s Doing Work\n",name)
		time.Sleep(250*time.Millisecond)

		if atomic.LoadInt64(&shutdown) == 1{
			fmt.Printf("shutdown %s\n",name)
			break
		}
	}
}
```

#### 互斥锁

1. 使用互斥锁mutex

```go
package main

import (
	"fmt"
	"runtime"
	"sync"
)
var(
	counter int
	wait sync.WaitGroup
	mutex sync.Mutex
)
func main() {
	wait.Add(2)
	go incCounter(1)
	go incCounter(2)
	wait.Wait()
	fmt.Println(counter)

}
func incCounter(id int)  {
	defer wait.Done()

	for count := 0; count < 2; count++ {
		mutex.Lock()
		{
			value := counter
			runtime.Gosched()
			value++
			counter = value
		}
		mutex.Unlock()
	}
}
```

#### 通道

1. 使用make创建通道

```go
//无缓冲的整型通道
unbuffered := make(chan,int)
//有缓冲的字符串通道
buffered := make(chan,string,10)
```

<<<<<<< Updated upstream
2. 无缓冲通道示例1：打球接力

```go
package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

var wg sync.WaitGroup

func init() {
	rand.Seed(time.Now().UnixNano())
}
func main() {
	court := make(chan int)
	wg.Add(2)
	go player("A", court)
	go player("B", court)
	court <- 1
	wg.Wait()

}
func player(name string, court chan int) {
	defer wg.Done()

	for true {
		ball, ok := <-court
		if !ok {
			fmt.Printf("Player %s Won\n", name)
			return
		}
		n := rand.Intn(100)
		if n%13 == 0 {
			fmt.Printf("Player %s Miss\n", name)
			close(court)
			return
		}
		fmt.Printf("Player %s Hit %d\n", name, ball)
		ball++
		court <- ball
	}

}
```

3. 无缓冲通道示例2：接力

```go
package main

import (
	"fmt"
	"sync"
	"time"
)

var wg sync.WaitGroup

func main() {
	baton := make(chan int)
	wg.Add(1)
	go Runner(baton)
	baton <- 1
	wg.Wait()
}

func Runner(baton chan int) {
	var newRunner int
	runner := <-baton
	fmt.Printf("Runner %d With Baton\n", runner)
	if runner != 4 {
		newRunner = runner + 1
		fmt.Printf("Runner %d To The Line\n", newRunner)
		go Runner(baton)
	}

	time.Sleep(1000 * time.Microsecond)

	if runner == 4 {
		fmt.Printf("Runner %d Finished,Race Over\n", runner)
		wg.Done()
		return
	}
	fmt.Printf("Runner %d Exchange With Runner %d\n", runner, newRunner)
	baton <- newRunner
}
```

4. 有缓冲通道示例1：消费问题

```go
package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

const (
	numberGoroutines = 4
	taskLoad         = 10
)
var wg sync.WaitGroup

func init() {
	rand.Seed(time.Now().Unix())
}
func main() {
	tasks := make(chan string, taskLoad)
	wg.Add(numberGoroutines)
	for gr := 1; gr <= numberGoroutines; gr++ {
		go worker(tasks, gr)
	}
	for post := 1; post <= taskLoad; post++ {
		tasks <- fmt.Sprintf("Task: %d", post)
	}
	close(tasks)
	wg.Wait()
}
func worker(tasks chan string, worker int) {
	defer wg.Done()
	for true {
		task, ok := <-tasks
		if !ok {
			fmt.Printf("Worker: %d : Shutting Down\n", worker)
			return
		}
		fmt.Printf("Worker: %d Started %s\n", worker, task)
		sleep := rand.Int63n(100)
		time.Sleep(time.Duration(sleep) * time.Microsecond)
		fmt.Printf("Worker : %d : Completed %s\n", worker, task)
	}
}
```
2. 向通道发送值

```go
buffered <- "Gopher"
```

3. 从通道接收一个值

```go
value := <- buffered
```

##### 有无缓冲通道的区别

​	**无缓冲的通道（unbuffered channel）：**是指在接收前没有能力保存任何值的通道。这种类型的通 道要求发送 goroutine 和接收 goroutine 同时准备好，才能完成发送和接收操作。如果两个goroutine 没有同时准备好，通道会导致先执行发送或接收操作的 goroutine 阻塞等待。这种对通道进行发送 和接收的交互行为本身就是同步的。其中任意一个操作都无法离开另一个操作单独存在。

