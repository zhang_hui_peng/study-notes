# Vue基础知识

## el挂载点

- el是用来设置Vue实例挂载（管理）的元素
- Vue会管理el选项命中的元素及内部的后代元素
- 可以使用其他的选择器，但是建议使用ID选择器
- 可以使用其他双标签，不能使用HTML和BODY

## data数据对象

- Vue中用到的数据定义在data中
- data中可以写复杂类型的数据
- 渲染复杂类型的数据时，遵循js的语法即可

## v-text指令

- v-text指令的作用是：设置标签的内容（textContent）
- 默认写法会替换全部内容，使用插值表达式{{}}可以替换指定内容
- 内部支持写表达式

```vue
<div id="app">
	<H2 v-text="message"></H2>
</div>
<script>
	var app = new Vue({
		el:"#app",
		data:{
			message:"hello"
		}
	})
</script>
```

## v-html指令

- v-html指令的作用是：设置元素的innerHTML
- 内容中有html结构会被解析成标签
- v-text指令无论内容是什么，只会解析为文本
- 解析文本使用v-text，解析html使用v-html

```vue
<div id="app">
	<H2 v-html="content"></H2>
</div>
<script>
	var app = new Vue({
		el:"#app",
		data:{
			content:"<a href='http://www.baidu.com'>百度</a>"
		}
	})
</script>
```

## v-on指令

- v-on指令的作用是：为元素绑定事件
- 事件名不需要写on
- 指令可以简写为@
- 绑定的方法定义在methods属性中
- 方法内部通过this可以访问定义在data中的数据

```vue
<div id="app">
	<input type="button" value="v-on指令" v-on:click="doIt">
	<input type="button" value="v-on指令" @click="changeText">
	<h2>{{text}}</h2>
</div>
<script>
	var app = new Vue({
		el:"#app",
		data:{
			text:"hello!"
		},
		methods:{
			doIt:function(){
				alert("coding");
			},
			changeText:function(){
				this.text+="hello!"
			}
		}
	})
</script>
```

