## istio架构

**istio**服务网格**逻辑**上分为**数据平面**和**控制平面**。

- **数据平面**由一组以**sidecar**方式部署的智能代理（**Envoy**）组成。这些代理可以调节和控制微服务及**Mixer**之间所有的网络通信。
- **控制平面**负责管理和配置代理来路由流量。此外控制平面配置**Mixer**以实施策略和收集遥测数据。

## 架构图

<img src="C:\Users\Administrator\Desktop\study-notes\笔记\images\image-20200212155907213.png" alt="image-20200212155907213" style="zoom: 80%;" />

## 相关组件

### Envoy

**istio**使用**Envoy**代理的扩展版本，**Envoy**是以**C++**开发的高性能代理，用于调解服务网格中所有服务的所有入站和出站流量。**Envoy**的许多内置功能被**Istio**发扬光大，例如：

- 动态服务发现
- 负载均衡
- TLS终止
- HTTP/2&gRPC代理
- 熔断器
- 健康检查、基于百分比流量拆分的灰度发布
- 故障注入
- 丰富的度量指标

**Envoy**被部署为**sidecar**，和对应服务在同一个**Kubernetes pod**中。这允许**istio**将大量关于流量行为的信号作为属性提取出来，而这些属性又可以在**Mixer**中用于执行策略决策，并发给监控系统，以提供整个网络行为的信息。

**Sidecar**代理模型还可以将**Istio**的功能添加到现有的部署中， 而无需重新构建或重写代码。

### Mixer

**Mixer**是一个独立于平台的组件，负责在服务网格上执行访问控制和使用策略，并从**Envoy**代理和其他服务收集遥测数据。代理提取请求级属性，发送到**Mixer**进行评估。

#### Mixer架构图

<img src="C:\Users\Administrator\Desktop\study-notes\笔记\images\image-20200212162530228.png" alt="image-20200212162530228" style="zoom:80%;" />

### Pilot

**Pilot**为**Envoy sidecar**提供服务发现功能，为智能路由（例如A/B测试、金丝雀部署等）和弹性（超时、重试、熔断器等）提供流量管理功能。它将控制流量行为的高级路由功能规则转化为特定于**Envoy**的配置，并在运行时将它们传播到**sidecar**

**Pilot** 将平台特定的服务发现机制抽象化并将其合成为符合 **Envoy数据平面API**的任何**sidecar**都可以使用的标准格式。这种松散耦合使得 **Istio** 能够在多种环境下运行（例如，**Kubernetes、Consul、Nomad**），同时保持用于流量管理的相同操作界面。

#### Pilot架构图

<img src="C:\Users\Administrator\Desktop\study-notes\笔记\images\image-20200212163100407.png" alt="image-20200212163100407" style="zoom:80%;" />

### Citadel

**Citadel**通过内置身份和凭证管理赋能强大的服务间和最终用户身份验证。可用于升级服务网格中未加密的流量，并为运维人员提供基于服务标识而不是网络控制的强制执行策略的能力。从 0.5 版本开始，**Istio **支持**基于角色的访问控制**，以控制谁可以访问您的服务，而不是基于不稳定的三层或四层网络标识。

#### Citadel架构图

<img src="C:\Users\Administrator\Desktop\study-notes\笔记\images\image-20200212163347070.png" alt="image-20200212163347070" style="zoom:80%;" />

**Galley** 代表其他的 **Istio** 控制平面组件，用来验证用户编写的 **Istio API** 配置。随着时间的推移，**Galley** 将接管 **Istio** 获取配置、 处理和分配组件的顶级责任。它将负责将其他的 **Istio 组件**与从底层平台（例如 **Kubernetes**）获取用户配置的细节中隔离开来。

