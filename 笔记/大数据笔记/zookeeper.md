## 配置内存使用大小

在zookeeper配置文件所在目录，添加java.env文件

```shell
export JVMFLAGS="-Xms4096m -Xmx4096m $JVMFLAGS"
```

然后重启zookeeper