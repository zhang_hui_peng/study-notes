# 全局自定义异常处理

## 自定义枚举类

```java
package com.geovis.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ErrorDefEnum {

    // 未知错误
    ERROR_UNKNOWN("00110000", "未知错误"),
    //参数判断
    ERROR_PARAMS("00110001", "请求参数异常！");

    private String code;

    private String message;

}

```



## 自定义异常类

```java
package com.geovis.handler;

import com.geovis.constant.ErrorDefEnum;

import org.springframework.http.HttpStatus;

import lombok.Data;
import lombok.EqualsAndHashCode;


@Data
@EqualsAndHashCode(callSuper = false)
public class ServiceException extends RuntimeException {

	private static final long serialVersionUID = -9061613031552007032L;

	private ErrorDefEnum errorDef;

	private HttpStatus httpStatus;
	
	public ServiceException(String message) {
		this(ErrorDefEnum.ERROR_UNKNOWN, message);
    }
	
	public ServiceException(ErrorDefEnum errorDef, String message) {
		this(errorDef, message, HttpStatus.BAD_REQUEST);
	}
	
	public ServiceException(ErrorDefEnum errorDef) {
		this(errorDef, HttpStatus.BAD_REQUEST);
	}

	public ServiceException(ErrorDefEnum errorDef, HttpStatus httpStatus) {
		this(errorDef, errorDef.getMessage(), httpStatus);
	}

	public ServiceException(ErrorDefEnum errorDef, String message, HttpStatus httpStatus) {
		super(message);
		this.errorDef = errorDef;
		this.httpStatus = httpStatus;
	}

}

```

## 自定义全局异常处理类

```java
package com.geovis.handler;

import java.sql.SQLTransientConnectionException;

import javax.servlet.http.HttpServletResponse;

import com.geovis.constant.ErrorDefEnum;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;


@ControllerAdvice
@ResponseBody
@Slf4j
public class ServiceExceptionHandler {

	@Data
	@AllArgsConstructor
	class ErrorReponse {
		private String code;
		private String message;
	}

	@ExceptionHandler(ServiceException.class)
	public ErrorReponse handleException(HttpServletResponse response, ServiceException ex) {
		response.setStatus(ex.getHttpStatus().value());
		ErrorDefEnum error = ex.getErrorDef();
		log.error("发生错误：code = {}, message = {}", error.getCode(), ex.getMessage());
		return new ErrorReponse(error.getCode(), ex.getMessage());
	}
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(SQLTransientConnectionException.class)
	public ErrorReponse handleSQLTransientConnectException() {
		log.error("数据库连接超时");
		return new ErrorReponse(ErrorDefEnum.ERROR_SQL_EXCEPTION.getCode(), ErrorDefEnum.ERROR_SQL_EXCEPTION.getMessage());
	}
}

```

