# SpringBoot笔记

## SpringBoot参数加载顺序

SpringBoot的参数配置会以下列的优先级顺序进行加载

- 命令行参数
- 来自**Java:comp/env**的**JNDI**属性
- **Java**系统属性(**System.getProperties()**)
- 操作系统变量
- **RandomValuePropertySource**配置的**random.***属性值
- **jar**包外部的**application-{profile}.propertie**或**application.yml**(带**spring.profile**)配置文件
- **jar**包内部的**application-{profile}.propertie**或**application.yml**(带**spring.profile**)配置文件
- **jar**包外部的**application.propertie**或**application.yml**(不带**spring.profile**)配置文件
- **jar**包内部的**application.propertie**或**application.yml**(不带**spring.profile**)配置文件
- **@Configuration**注解类上的**@PropertySource**
- 通过**SpringApplication.setDefaultProperties**指定的默认属性

## Spring Boot五大特性

- SpringApplication
- 自动装配
- 外部化配置
- Spring Boot Actuator
- 嵌入式Web容器

## Spring Boot常用注解

- ==@Bean==：是一个可以使用在方法上的注解，可以通过设置name=xxx设置bean的name，不设置默认是返回的方法的小写。
- ==@Scope==：设置生成的bean单例还是多例，默认为单例
- ==@Configuration==：相当于spring的xml当中的<beans>标签
- ==@Primary==：可以在一个类或者接口有两个继承或者实现类的时候，在自动注入时选择标注@Primary这个标签的类，消除歧义性。
- ==@Qualifier==：当需要在方法中进行依赖注入的时候，如果有多个匹配类型，使用@Qualifier来指定注入的bean
- ==@Resources==：直接通过名字指定注入的bean
- ==@GetMapping==：是一个组合注解 是@RequestMapping(method = RequestMethod.GET)的缩写
- ==@PostMapping==：是一个组合注解 是@RequestMapping(method = RequestMethod.POST)的缩写
- ==@PutMapping==：是一个组合注解 是@RequestMapping(method = RequestMethod.PUT)的缩写
- ==@DeleteMapping==：是一个组合注解 是@RequestMapping(method = RequestMethod.DELETE)的缩写
- ==@PatchMapping==：是一个组合注解 是@RequestMapping(method = RequestMethod.PATCH)的缩写

## Runner

### ApplicationRunner

在应用启动的时候执行，比**CommandLineRunner**优先执行

```java
@Bean
public ApplicationRunner runner(WebServerApplicationContext applicationContext) {
        return args -> {
     		System.out.println(webServerApplicationContext.getWebServer()
                        .getClass().getName());
     };
}
```

多个实现类的执行顺序：

- 使用**@Order(value=整数值)**
- 实现**Ordered**接口，在方法里**return**一个顺序值

### CommandLineRunner

在应用启动的时候执行，比**ApplicationRunner**延后执行

```java
@Bean
public CommandLineRunner commandLineRunner(WebServerApplicationContext applicationContext) {
        return args -> {
            System.out.println(webServerApplicationContext.getWebServer().getPort());
    };
}
```

多个实现类的执行顺序：

- 使用**@Order(value=整数值)**
- 实现**Ordered**接口，在方法里**return**一个顺序值

## @EventListener

使用事件监听器监听**WebServer**是否初始化完成

```java
@EventListener(WebServerInitializedEvent.class)
public void onWebServerReady(WebServerInitializedEvent event) {
        System.out.println("当前WebServer实现类为: " +       			      event.getWebServer().getClass().getName());
}
```

## @AliasFor

使用**@AliasFor**注解能够将一个或多个注解的属性"别名"在某个注解中。

==@SpringBootApplication==中的实现

```java
@AliasFor(annotation = ComponentScan.class, attribute = "basePackages")
String[] scanBasePackages() default {};
```

## @Configuration和@Component

被标注了**@Configuration**注解的类会被**CGLIB**增强，而标注**@Component**则不会

## @Conditional注解

使用**@Conditional**注解来动态配置**Bean**，只有指定的类返回的**ConditionOutcome**类为**true**时，这个配置了**@ConditionalOnBean**注解的**Bean**才会被注册进**IOC**容器

```java
@Configuration
@Conditional(WrongConditional.class)
@Import(FinalImportClass.class)
static class ImportClass {

}

static class WrongConditional extends SpringBootCondition {
      @Override
      public ConditionOutcome getMatchOutcome(ConditionContext context, AnnotatedTypeMetadata metadata) {

          return new ConditionOutcome(true, "value is false");
      }
}
```



## @ConditionalOnBean注解

使用**@ConditionalOnBean**注解来动态配置**Bean**，只有指定的**Bean**在**IOC**容器中存在，这个配置了**@ConditionalOnBean**注解的**Bean**才会被注册进行**IOC**容器

```java
@Configuration
@ConditionalOnBean(FinalImportClass.class)
static class TestConditionalOnBean {

}
```



## @ConditionalOnClass注解

使用**@ConditionalOnClass**注解来动态配置**Bean**，只有指定的**Class**在当前的**ClassPath**中存在，这个配置了**@ConditionalOnBean**注解的**Bean**才会被注册进行**IOC**容器

```java
@Configuration
@ConditionalOnClass(Test.class)
static class TestConditionalOnClass {

}
```



## @EnableConfigurationProperties和@ConfigurationProperties的使用

当使用**@ConfigurationProperties**时，会将配置文件的信息转化成一个**Bean**，如果不使用**@Component（或@Configuration）**注解，则不会将这个**Bean**注册进**IOC**容器。使用**@EnableConfigurationProperties**注解可以在不加**@Component（或@Configuration）**注解的情况下将这个**Bean**注入到**IOC**容器中。

```java
//配置类
@ConfigurationProperties(prefix = "zhangsan")
public class ComfigurationYML {
    String name;
    String age;
}

//导入配置类
@Configuration
@EnableConfigurationProperties(ComfigurationYML.class)
public class TestConfiguration {
    @Autowired
    ComfigurationYML comfigurationYML;
}
```



## SpringBoot自动装配原理

> spring-boot-autoconfigure是Spring Boot核心模块（JAR），其中提供了大量的内建自动装配@Configuration类，它们统一存放在org.springframework.boot.autoconfigure包或子包下，比如DataSourceAutoConfiguration的包名为org.springframework.boot.autoconfigure.jdbc。同时，这些类均配置在META-INF/spring.factories资源中。

自定义自定配置类只需要项目的**src/main/resources**的目录下新建**META-INF/spring.factories**资源，并配置自定义的自动配置类

```java
//自定义自动配置类
@Configuration
@Import(WebConfiguration.class)
public class WebAutoConfiguration {

}

//META-INF/spring.factories文件内容
org.springframework.boot.autoconfigure.EnableAutoConfiguration=\
  com.springboot.learningdemo.autoconfigure.WebAutoConfiguration
```

 