# Actuator

actuator中常用的**Endpoints**：

- ==beans==：显示当前**Spring**应用上下文的**Spring Bean**的完整列表（包含所有**ApplicationContext**的层次）
- ==conditions==：显示当前应用所有配置类和自动装配类的条件评估结果（包含匹配和非匹配）
- ==env==：暴露**Spring ConfigurableEnvironment**中的**PropertySource**属性
- ==health==：显示应用的健康信息
- ==info==：显示任意的应用信息

其中，**health**和**info**为默认暴露的**Web Endpoints**，如果需要暴露其他**Endpoints**，则可增加==management.endpoints.web.exposure.include=*==的配置属性到application.properties或启动参数中。