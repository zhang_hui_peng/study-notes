# Spring IOC

## Spring IOC依赖来源

- 自定义Bean
- 容器内建Bean对象（==Environment==）
- 容器内建依赖（==BeanFactory 例如： DefaultListableBeanFactory==）

## IOC配置元信息

- Bean定义配置
  - 基于XML文件
  - 基于Properties文件配置
  - 基于Java注解
  - 基于Java API
- IOC容器配置
  - 基于XML文件
  - 基于Java注解
  - 基于Java API
- 外部化属性配置
  - 基于Java注解

## BeanFactory和ApplicationContext

> BeanFactory和ApplicationContext其实是同一类事物，都复用了同一个接口：BeanFactory，只不过在底层实现的时候ApplicationContext组合了一个BeanFactory的实现。
>
> - BeanFactory是Spring底层IOC容器
> - ApplicationContext是具备应用特性的BeanFactory超集

```java
public abstract class AbstractRefreshableApplicationContext extends AbstractApplicationContext {

	@Nullable
	private Boolean allowBeanDefinitionOverriding;

	@Nullable
	private Boolean allowCircularReferences;

	/** Bean factory for this context. */
	@Nullable
	private DefaultListableBeanFactory beanFactory;
    @Override
	public final ConfigurableListableBeanFactory getBeanFactory() {
		synchronized (this.beanFactoryMonitor) {
			if (this.beanFactory == null) {
				throw new IllegalStateException("BeanFactory not initialized or already 		closed - " +"call 'refresh' before accessing beans via the ApplicationContext");
			}
			return this.beanFactory;
		}
	}
}
```

ApplicationContext除了IOC容器角色，还提供：

- 面向切面（AOP）
- 配置元信息（Configuration Metadata）
- 资源管理（Resources）
- 事件（Events）
- 国际化（i18n）
- 注解（Annotations）
- Environment抽象（Environment Abstraction）

## IoC容器生命周期

- 启动
- 运行
- 停止

## 定义Spring Bean

==BeanDefinition==是Spring Framework中定义Bean的配置元信息接口，包括：

- Bean的类名
- Bean行为配置元素，如作用域、自动绑定的模式，生命周期的回调等
- 其他Bean引用，又可称作合作者（collaborators）或者依赖（dependencies）
- 配置设置，比如Bean属性（Properties）

**BeanDefinition元信息**

| 属性（Property）        | 说明                                         |
| ----------------------- | -------------------------------------------- |
| Class                   | Bean全类名，必须是具体类，不能用抽象类或接口 |
| Name                    | Bean的名称或者ID                             |
| Scope                   | Bean的作用域（如：singleton、prototype等）   |
| ConStructor arguments   | Bean构造器参数（用于依赖注入）               |
| Properties              | Bean属性设置（用于依赖注入）                 |
| Autowiring mode         | Bean自动绑定模式（如：通过名称byName）       |
| Lazy initialzation mode | Bean延迟初始化模式（延迟和非延迟）           |
| initialzation method    | Bean初始化回调方法名称                       |
| Destruction method      | Bean销毁回调方法名称                         |

## 注册Spring Bean

==BeanDefinition注册==：

- XML配置文件

- Java注解配置元信息
  - @Bean
  - @Component
  - @Import
  
- Java API 配置元信息
  - 命名方式：BeanDefinitionRegistry#registerBeanDefinition(String，BeanDefinition)
  
  ```java
  AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
          applicationContext.refresh();
          BeanDefinitionBuilder beanDefinitionBuilder = BeanDefinitionBuilder.genericBeanDefinition(User.class);
          beanDefinitionBuilder.addPropertyValue("name", "lisi");
          beanDefinitionBuilder.addPropertyValue("age", 11);
          applicationContext.registerBeanDefinition("user",beanDefinitionBuilder.getBeanDefinition());
  ```
  
  - 非命名方式：BeanDefinitionReaderUtils#registerWithGeneratedName(AbstractBeanDefinition,BeanDefinitionRegistry)
  
  ```java
   BeanDefinitionReaderUtils.registerWithGeneratedName(beanDefinitionBuilder.getBeanDefinition(),applicationContext);
  ```
  
  
  
  - 配置类方式：AnnotatedBeanDefinitionReader#register(Class...)

## 实例化Spring Bean

### 常规方法

- 通过构造器（配置元信息：XML、Java注解和Java API）
- 通过静态工厂方法（配置元信息：XML和Java API）
- 通过Bean工厂方法（配置元信息：XML和Java API）
- 通过FactoryBean（配置元信息：XML、Java注解和Java API）

### 特殊方式

- 通过ServiceLoaderFactoryBean（配置元信息：XML 、Java注解和Java API)
- 通过AutowiredCapableBeanFactory#createBean(java.lang.Class, int, boolean)
- 通过BeanDefinitionRegistry#registerBeanDefinition(String, BeanDefinition)

## 初始化Spring Bean

### 初始化的三种方式

- **@PostConstruct** 标注方法
- 实现**initializingBean**接口的**afterPropertiesSet**方法
- 自定义初始化方法
  - XML配置：<bean init-method="init..."  >
  - Java注解：@Bean(initMethod = "init...")
  - Java API：AbstractBeanDefinition#setInitMethodName(String)

**如果以上三种方式均在一个Bean中定义，初始化顺序为**

```java
@PostConstruct > 实现initializingBean接口 > 自定义初始化方法
```

### 延迟初始化与非延迟初始化

非延迟初始化是在上下文启动之前初始化（applicationContext容器初始化过程中进行初始化），延迟初始化是在上下文启动之后初始化。

## 销毁Spring Bean

### Bean销毁的三种方式

- **@PreDestory**标注方法
- 实现**DisposableBean**接口的**destroy**方法
- 自定义销毁方法
  - XML配置：<bean destroy="destroy...">
  - Java注解：@Bean(destroy="destroy")
  - Java API：AbstractBeanDefinition#setDestroyMethodName(String)

**如果以上三种方式均在一个Bean中定义，销毁顺序为**

```java
@PreDestroy > 实现DisposableBean接口 > 自定义销毁方法
```

### 