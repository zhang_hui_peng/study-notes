# git的使用

## git的配置

### 配置用户名邮箱

```shell
git config --global user.name "..."
```

```shell
git config --global user.email "..."
```

## git理论

### 工作区域

Git本地有三个工作区域：==工作目录（Working Directory）==、==暂存区（Stage/Index）==、==资源库（Repository）==，如果再加上远程的git仓库（==Remote Directory==）就可以分成四个工作区域。文件在这四个区域之间的转换关系如下：

![image-20200626234428892](./images/image-20200626234428892.png)

- WorkSpace：工作区，就是平时存放项目代码的地方
- Index/Stage：暂存区，用于临时存放你的改动，事实上它只是一个文件，保存即将提交到文件列表信息
- Repository：仓库区（或本地仓库），就是安全存放数据的位置，这里面有你提交到所有版本的数据。其中==HEAD==指向最新放入仓库的版本
- Remote：远程仓库，托管代码的服务器，可以简单的认为是你项目组中的一台电脑用于远程数据交换

## git使用的大概流程

日常工作只要记住下图6个命令

![image-20200626235247710](./images/image-20200626235247710.png)

## git文件操作

### 文件的四种状态

- ==Untracked==：未跟踪，此文件再文件夹中，但并没有加入到git库，不参与版本控制。通过==git add==状态变为==Staged==
- ==Unmodify==：文件已入库，未修改，即版本库中的文件快照内容与文件夹中完全一致，这种类型的文件有两种去处，如果它被修改，而变为==Modified==，如果使用==git rm==移出版本库，则成为==Untracked==文件。
- ==Modified==：文件已修改，仅仅是修改，并没有进行其他操作。这个文件也有两个去处，通过==git add==可进入到暂存==Staged==状态，使用==git checkout==则丢弃修改，返回到==Unmodify==状态，这个==git checkout==即从库中取出文件，覆盖当前修改。
- ==Staged==：暂存状态，执行==git commit==则将修改同步到库中，这时库中的文件和本地文件又变为一致，文件为==Unmodify==状态。执行==git reset HEAD filename==取消暂存，文件状态为==Modified==

### 查看文件状态

```shell
#查看指定文件状态
git status filename

#查看所有文件状态
git status
```



## git命令

### 创建本地仓库

1、创建全新仓库

```shell
git init
```

2、克隆远程仓库

```shell
git clone [URL]
```

3、丢弃工作区的改动

```shell
git checkout filename
```

4、取消暂存，保留更改

```shell
git reset HEAD filename
```

5、列出本地所有分支

```shell
git branch
```

6、列出所有远程分支

```shell
git branch -r
```

7、新建一个分支，但依然停留在当前分支

```shell
git branch [branch-name]
```

8、切换分支

```shell
git checkout [branch-name]
```

9、新建一个分支，并切换到该分支

```shell
git checkout -b [branch-name]
```

10、合并指定分支到当前分支

```shell
git merge [branch-name]
```

11、删除分支

```shell
git branch -d [branch-name]
```

12、删除远程分支

```shell
git push origin --delete [branch-name]
git branch -dr [remote/branch-name]
```



