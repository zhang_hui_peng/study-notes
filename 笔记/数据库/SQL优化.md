# SQL优化

## 索引

### 索引的分类

- **单值索引**
- **唯一索引**
- **复合索引**
- **主键索引**（与唯一索引类似，但值不能为空，唯一索引值可以为空）

### 添加索引

添加单值索引：

```sql
CREATE INDEX name_index ON student(name);
```

 添加唯一索引；

```sql
CREATE UNIQUE INDEX name_index ON student(name);
```

添加复合索引：

```sql
CREATE INDEX name_index ON student(name,age);
```

### 删除索引

```sql
DROP INDEX name_index on student;
```

### 查询索引

```sql
SHOW INDEX FROM student;
```

### 查询执行计划

```sql

```

