# Redis

## 五种数据类型

- ==String==	

  内部编码

  - raw
  - int
  - embstr

| GET  | 获取存储在给定键中的值                         |
| ---- | ---------------------------------------------- |
| SET  | 设置存储在键中的值                             |
| DEL  | 删除存储在给定键中的值（该命令适用于所有类型） |

- ==List==	

  内部编码

  - linkedlist
  - ziplist

| LPUSH  | 将元素推入到列表的左端             |
| ------ | ---------------------------------- |
| RPUSH  | 将元素推入到列表的右端             |
| LPOP   | 将元素从列表的左端弹出             |
| RPOP   | 将元素从列表的右端弹出             |
| LINDEX | 用于获取列表在给定位置上的一个元素 |
| LPOP   | 获取列表在给定范围上的所有值       |

- ==Set==

  内部编码：

  - hashtable
  - intset

| SADD      | 将给定元素添加到集合                       |
| --------- | ------------------------------------------ |
| SMEMBERS  | 返回集合包含的所有元素                     |
| SISMEMBER | 返回给定元素是否存在于集合中               |
| SREM      | 如果给定元素存在于集合中，那么移除这个元素 |

- ==Hash==

  内部编码

  - hashtable
  - ziplist

| HSET    | 在散列里面关联起给定的键值对             |
| ------- | ---------------------------------------- |
| HGET    | 获取指定散列键的值                       |
| HGETALL | 获取散列包含的所有键值对                 |
| HDEL    | 如果给定键存在于散列里面，那么移除这个键 |

- ==ZSet==

  内部编码

  - skiplist
  - ziplist

| ZADD          | 将一个带有给定分值的成员添加到有序集合里面             |
| ------------- | ------------------------------------------------------ |
| ZRANGE        | 根据分值的排序顺序，获取有序集合在给定范围内的所有元素 |
| ZRANGEBYSCORE | 获取有序集合在给定分值范围内的所有元素                 |
| ZREM          | 如果给定成员存在于有序集合，那么移除这个成员           |

## 通用命令

| TYPE            | 查询存储在键中的值的类型          |
| --------------- | --------------------------------- |
| EXPIRE          | 设置键的过期时间                  |
| TTL             | 查询键的剩余时间                  |
| OBJECT ENCODING | 查询给定键所对应值的内部编码      |
| RENAME          | 键重命名                          |
| RENAMENX        | 当newKey不存在时，才会成功        |
| RANDOMKEY       | 随机返回一个键                    |
| expireat        | 设置键在秒级时间戳timestamp后过期 |
| persist         | 可以将键的过期时间清除            |
| MOVE            | 迁移键到另一个库                  |
| KEYS            | 遍历键                            |
| SCAN            | 渐进式遍历键                      |
| SELECT          | 切换数据库                        |
| FLUSHDB         | 清除当前数据库                    |
| FLUSHALL        | 清除所有数据库                    |

## String（字符串类型）

​	字符串类型是Redis最基础的数据结构。键都是字符串类型，字符串类型的值实际可以是**字符串（简单的字符串、复杂的字符串（例如JSON、XML））、数字（整数、浮点数），甚至是二进制（图片、音频、视频）**，但是值最大不超过**512MB**

### 常用命令

- **set**设置值

  命令选项

  - ex second：为键设置秒级过期时间
  - px milliseconds：为键设置毫秒级过期时间
  - nx：键必须不存在，才可以设置成功，用于添加
  - xx：与nx相反，键必须存在，才可以设置成功，用于更新。

- setex：与set的ex的选项一样

- setnx：与set的nx的选项一样，可用作分布式锁

- get：获取值

- mset：批量设置值

- mget：批量获取值

- incr：自增操作

- dect：自减

- decrby：自减指定数字

- incrbyfloat：自增浮点数

### 不常用命令

- append：向字符串尾部追加值
- strlen：字符串长度
- getset：设置并返回原值
- setrange：设置指定位置的字符
- getrange：获取部分字符串

### 内部编码

- int：8个字节的长整形
- embstr：小于等于39个字节的字符串
- raw：大于39个字节的字符串

redis会根据当前值的类型和长度决定使用哪种内部编码实现

## HASH（哈希）

### 常用命令

- hset：设置值
- hsetnx：如同setnx命令
- hget：获取值
- hdel：删除field
- hlen：计算field个数
- hmget：批量获取field-value
- hmset：批量设置field-value
- hexists：判断field是否存在
- hkeys：获取所有field
- hvals：获取所有的value
- hgetall：获取所有的field-value
- hstrlen：计算value的字符串长度
- hincrby和hincrbyfloat：类似incrby和incrbyfloat命令

### 内部编码

- ziplist：压缩列表，当哈希类型元素个数小于**hash-max-ziplist-entries**配置（默认==512==个）、同时所有值都小于**hash-max-ziplist-value**配置（默认==64==字节）时，Redis会使用ziplist作为哈希的内部实现，ziplist使用更加紧凑的结构实现多个元素的连续存储，所以在节省内存方面比hashtable更加优秀
- hashtable：哈希表，当哈希类型无法满足ziplist的条件时，Redis会使用hashtable作为哈希的内部实现，因为此时ziplist的读写效率会下降，而hashtable的读写时间复杂度为O(1)

## List（列表）

​	列表类型是用来存储多个有序的字符串，一个列表最多存储==2^32^-1==个元素，列表是一种比较灵活的数据结构，它可以充当栈和队列的角色。

### 常用命令

- rpush：从右边插入元素
- lpush：从左边插入元素
- lrange：从左到右获取列表的中索引范围内的元素
- linsert：向某个元素前或者后插入元素
- lindex：获取列表指定索引下标的元素
- llen：获取列表长度
- lpop：从列表左侧弹出元素
- rpop：从列表右侧弹出元素
- lrem：删除指定元素
- ltrim：按照索引范围修剪列表
- lset：修改指定索引下标的元素
- blpop和brpop：lpop和rpop的阻塞版本，使用brpop时，如果是多个键，那么brpop会从左到右遍历键，一旦有一个键能弹出元素，客户端立即返回。

### 内部编码

- ziplist：压缩列表，当列表的元素个数小于**list-max-ziplist-entries**（默认==512==个），同时列表中每个元素的值都小于**list-max-ziplist-value**（默认==64==字节），Redis会选用ziplist来作为列表的内部实现来减少内存的使用。
- linkedlist：链表，当列表类型无法满足ziplist的条件时，Redis会使用linkedlist作为列表的内部实现。

## Set（集合）

​	集合类型也是用来保存多个的字符串元素，但和列表类型不一样的是，集合中不允许有重复元素，且集合总的元素是无序的，不能通过索引下标获取元素。一个集合最多可以存储==2^32^-1个元素。Redis除了支持集合内的增删改查，同时还支持多个集合取交集、并集、差集。

### 常用命令

- sadd：添加元素
- srem：删除元素
- scard：计算元素个数
- sismember：判断元素是否在集合中
- srandmember：随机从集合中返回指定个数元素
- spop：从集合中随机弹出元素
- smembers：获取所有元素
- sinter：求多个集合的交集
- sunion：求多个集合的并集
- sdiff：求多个集合的差集
- sinterstore、suionstore和sdiffstore：将交集、并集、差集的结果保存

### 内部编码

- intset：整数集合，当集合中的元素都是整数且元素个数小于**set-max-intset-entries**（默认==512==个）时，Redis会选用intset来作为集合的内部实现，从而减少内存的使用。
- hashtable：哈希表，当集合类型无法满足intset的条件时，Redis会使用hashtable作为集合的内部实现。

## ZSet（有序集合）

​	有序集合保留了集合不能有重复成员的特性，但不同的是，有序集合中的元素可以排序。但它和列表使用索引下标作为排序依据不同的是，它给每个元素设置一个分数（score）作为排序的依据。有序集合中的元素不能重复，但是score可以重复。

### 常用命令

- zadd：添加成员

  参数选项：

  - nx：member必须不存在，才可以设置成功，用于添加。
  - xx：member必须存在，才可以设置成功，用于更新。
  - ch：返回此次操作后，有序集合元素和分数发生变化的个数
  - incr：对score做增加，相当于后面介绍的zincrby

- zcard：计算成员个数

- zscore：计算某个成员的分数

- zrank：计算成员排名，从低到高。

- zrevrank：计算成员排名，从高到低。

### 内部编码

- ziplist：压缩列表，当有序集合的元素个数小于**zset-max-ziplist-entries**（默认==128==个）同时每个元素个数的值都小于**zset-max-ziplist-value**（默认==64==字节）时，Redis会用ziplist来作为有序集合的内部实现，ziplist可以有效减少内存的使用。
- skiplist：跳跃表，当ziplist条件不满足时，有序集合会使用skiplist作为内部实现，因为此时ziplist的读写效率会下降。

## 慢查询分析

### 慢查询的两个配置参数

​	Redis提供了**slowlog-log-slower-than**和**slowlog-max-len**配置来进行设置。**slowlog-log-slower-than**是预设阀值，单位是微妙，默认值是==10000==。Redis使用一个列表来存储慢查询日志，**slowlog-max-len**就是列表的最大长度。当有一个新的命令满足慢查询条件且列表已经处于最大长度时，最早插入的一个命令将从列表中移出。

- 获取慢查询日志：

  ```shell
  slowlog get [n]
  ```

- 获取慢查询日志列表长度

  ```shell
  slowlog len
  ```

- 慢查询日志重置

  ```shell
  slowlog reset
  ```

## Redis事务

​	Redis提供了简单的事务功能，将一组需要 一起执行的命令放到==multi==和==exec==两个命令之间。==multi==命令代表事务开始，==exec==命令代表事务结束，它们之间的命令是原子顺序执行的。

### 事务出错

1. 命令错误：例如将set命令写成了sett，属于语法错误，会造成整个事务无法执行，key和counter值未发生变化。
2. 运行时错误：如果在添加操作时，误把sadd命令写成了zadd命令，这种就是运行是错误，因为语法是正确的，Redis不支持回滚功能，前一个命令已经执行成功。

> 有些应用场景需要在事务之前，确保事务中的key没有被其他客户端修改过，才执行事务，否则不执行（类似乐观锁）。Redis提供了==watch==命令来解决这类问题。

## Redis持久化

### RDB

​	RDB持久化是通过==快照==的方式，即在指定的时间间隔内将内存中的数据集快照写入磁盘。在创建快照之后，用户可以备份该快照，可以将快照复制到其他服务器以创建相同数据的服务器副本，或者在重启服务器后回复数据。RDB是Redis的默认持久化方式。

#### 快照

​	RDB持久化会生成RDB文件，该文件是一个**压缩**过的**二进制文件**，可以通过该文件还原快照时的数据库状态，即生成该RDB文件时的服务器数据。RDB文件默认为当前工作目录下的==dump.rdb==

```xml
#设置dump的文件名
dbfilename dump.rdb

#工作目录
dir ./
```

#### 触发快照的时机

- 执行==save==和==bgsave==命令
- 配置文件设置==save <seconds> <changes>==规则，自动间隔性执行==bgsave==命令
- 主从复制时，从库全量复制同步主库数据，主库会执行==bgsave==
- 执行==flushall==命令清空服务器数据
- 执行==shutdown==命令关闭Redis时，会执行==save==命令

#### save和bgsave命令

执行==save==和==bgsave==命令的区别：

- 使用==save==命令会阻塞Redis服务器进程，服务器进程在RDB文件创建完成之前是不处理任何的命令请求
- 而使用==bgsave==命令不同的是，==bgsave==命令会==fork==一个子进程，然后该子进程会负责创建RDB文件，而服务器进程会继续处理命令请求

<img src="..\images\image-20200428102715415.png" alt="image-20200428102715415" style="zoom:80%;" />

在配置文件中设置==save <seconds> <changes>==规则（表示在seconds秒内，至少有changes次变化，就会自动触发`bgsave`命令），可以自动间隔性执行==bgsave==命令

```xml
save 900 1
save 300 10
save 60 10000
```

### AOF

​	除了RDB持久化，Redis还提供了==AOF（Append Only File）==持久化功能，AOF持久化会把被执行的写命令写到==AOF文件==的末尾，记录数据的变化。**默认情况下，Redis是没有开启AOF持久化的**，开启后每执行一条更改Redis数据的命令，都会把该命令追加到AOF文件中，这是会降低Redis的性能，但大部分情况下这个影响是能够接受的，另外使用较快的硬盘可以提高AOF的性能。

​	可以通过配置`redis.confi`文件开启AOF持久化

```xml
# appendonly参数开启AOF持久化
appendonly no

# AOF持久化的文件名，默认是appendonly.aof
appendfilename "appendonly.aof"

# AOF文件的保存位置和RDB文件的位置相同，都是通过dir参数设置的
dir ./

# 同步策略
# appendfsync always
appendfsync everysec
# appendfsync no

# aof重写期间是否同步
no-appendfsync-on-rewrite no

# 重写触发配置
auto-aof-rewrite-percentage 100
auto-aof-rewrite-min-size 64mb

# 加载aof出错如何处理
aof-load-truncated yes

# 文件重写策略
aof-rewrite-incremental-fsync yes
```

#### AOF的实现

​	AOF需要记录Redis的每个写明了，步骤为：命令追加（append）、文件写入（write）和文件同步（sync）

##### 命令追加（append）

开启AOF持久化功能后，服务器每执行一个命令，都会把该命令以协议的格式先追加到==aof_buf==缓冲区的末尾，而不是直接写入文件，避免每次有命令都直接写入硬盘，减少硬盘IO次数。

##### 文件写入（write）和文件同步（sync）

对于何时把==aof_buf==缓冲区的内容写入保存在AOF文件中，Redis提供了多种策略

- **appendfsync always**：将==aof_buf==缓冲区的所有内容写入并同步到AOF文件，每个写命令同步写入磁盘
- **appendfsync everysec**：将==aof_buf==缓冲区的内容写入AOF文件，默认每秒执行一次同步，该操作由一个线程专门负责
- **appendfsync no**：将==aof_buf==缓冲区的内容写入AOF文件，什么时候同步由操作系统来决定。

关于AOF的同步策略是涉及到操作系统的`write`函数和`fsync`函数的，在《Redis设计与实现》中是这样说明的：

> 为了提高文件写入效率，在现代操作系统中，当用户调用`write`函数，将一些数据写入文件时，操作系统通常会将数据暂存到一个内存缓冲区里，当缓冲区的空间被填满或超过了指定时限后，才真正将缓冲区的数据写入到磁盘里。
>
> 这样的操作虽然提高了效率，但也为数据写入带来了安全问题：如果计算机停机，内存缓冲区中的数据会丢失。为此，系统提供了`fsync`、`fdatasync`同步函数，可以强制操作系统立刻将缓冲区中的数据写入到硬盘里，从而确保写入数据的安全性。

#### AOF重写

​	AOF重写的目的就是减少AOF文件的体积，不过值得注意的是：**AOF文件重写并不需要对现有的AOF文件进行任何读取、分享和写入操作，而是通过读取服务器当前的数据库状态来实现的**。

##### 手动触发重写

​	手动触发执行==bgrewriteaof==命令，该命令的执行跟==bgsave==触发快照时类似，都是先==fork==一个子进程做具体的工作

##### 自动触发重写

​	自动触发会根据==auto-aof-rewrite-percentage==和==auto-aof-rewirte-min-size 64mb==配置来自动执行==bgrewriteaof==命令

```xml
# 表示当AOF文件的体积大于64MB，且AOF文件的体积比上一次重写后的体积大了一倍（100%）时，会执行`bgrewriteaof`命令
auto-aof-rewrite-percentage 100
auto-aof-rewrite-min-size 64mb
```

<img src="..\images\image-20200428105736382.png" alt="image-20200428105736382" style="zoom:80%;" />

## 数据恢复

​	Redis4.0开始支持RDB和AOF的**混合持久化**（可以通过配置==aof-use-rdb-preamble==开启）

- 如果redis进程挂掉，那么重启redis进程既可，直接基于AOF日志文件恢复数据
- 如果redis进程所在机器挂掉，那么重启机器后，尝试重启redis进程，尝试直接基于AOF日志文件进行恢复，如果AOF文件破损，那么用==redis-check-aof fix==命令恢复
- 如果没有AOF文件，会去加载RDB文件
- 如果redis当前罪行的AOF的RDB文件出现丢失/损坏，那么可以尝试基于该机器上当前的某个族姓的RDB数据副本进行数据恢复

## RDB vs AOF

### RDB和AOF优缺点

#### RDB

优点：

- RDB快照是一个压缩过的非常紧凑的文件，保存着某个时间点的数据集，适合做数据备份，灾难恢复
- 可以最大化Redis的性能，在保存RDB文件时服务器只需要==fork==一个子进程来完成RDB文件的创建，父进程不需要做IO操作
- 与AOF相比，恢复打数据集的时候会更快

缺点：

- RDB的数据安全性是不如AOF的，保存整个数据集的过程是比较繁重的，根据配置可能要几分钟才快照一次，如果服务器宕机，那么就可能丢失几分钟的数据
- Redis数据集较大时，==fork==的子进程要完成快照会比较耗CPU、耗时

#### AOF

优点：

- 数据更完整，安全性更高，秒级数据丢失（取决**fsync**策略，如果是**everysec**，最多丢失1秒的数据）
- AOF文件是一个只进行追加的日志文件，且写入操作是以Redis协议的格式保存的，内容是可读的，适合误删紧急恢复

缺点：

- 对于相同的数据集，AOF文件的体积要大于RDB文件，数据恢复也会比较慢
- 根据所使用的fsync策略，AOF的熟读可能会慢于RDB。不过在一般情况下，每秒fsync的性能依然非常高

## 如何选择持久化方式

- 如果是数据不那么敏感，且可以从其他地方重新生成补回，那么可以关闭持久化
- 如果是数据比较重要，不想再从其他地方获取，且可以承受数分钟的数据丢失，比如缓存等，那么可以只使用RDB
- 如果是用作内存数据库，要使用Redis的持久化，建议是RDB和AOF都开启，或者定期执行==bgsave==做快照备份，RDB方式更适合做数据的备份，AOF可以保证数据的不丢失。