---
typora-copy-images-to: ..\images
---

# JVM笔记

## 基本概念

1. **Class常量池**：可以理解为是Class文件中的资源仓库，用于存放编译器生成的各种**字面量(Literal)**和**符号引用(Symbolic References)**。Class文件的常量池入口处会设置两个字节的常量池容量计数器，记录了常量池中常量的个数

2. **字面量**：**字面量就是指由字母、数字等构成的字符串或者数值。**字面量只可以右值出现，如下：123和hello就是字面量

   ```java
   int a= 123,String s = "hello"
   ```

3. **符号引用**：符号引用是编译原理中的概念，是相对于直接引用来说的，主要包括三类常量：

   - 类和接口的全限定名
   - 字段的名称和描述符
   - 方法的名称和描述符

```java
Java代码在进行Javac编译的时候，并不像C和C++那样有“连接”这一步骤，而是在虚拟机加载Class文件的时候进行动态连接。也就是说，在Class文件中不会保存各个方法、字段的最终内存布局信息，因此这些字段、方法的符号引用不经过运行期转换的话无法得到真正的内存入口地址，也就无法直接被虚拟机使用。当虚拟机运行时，需要从常量池获得对应的符号引用，再在类创建时或运行时解析、翻译到具体的内存地址之中。关于类的创建和动态连接的内容，在虚拟机类加载过程时再进行详细讲解。
```

String的intern方法：在JDK6中，intern()方法会把首次遇到的字符串实例复制到永久代中，返回的也是永久代中这个字符串实例的引用，而JDK7的intern()实现不会再复制实例，只是在常量池中记录首次出现的实例引用。



## 相关参数

- **-Xms**：设置堆的最小值
- **-Xmx**：设置堆的最大值
- **-XX:+HeapDumpOnOutOfMemoryError**：让虚拟机在出现内存溢出异常时Dump出当前的内存堆转储快照
- **-Xss**：设置栈内存容量
- **-XX:PermSize=10M（1.8之后失效，永久代被元空间替换）**：方法区的内存大小
- **-XX:MaxPermSize=10M（1.8之后失效，永久代被元空间替换）**：设置方法区内存最大值
- **-XX:MetaspaceSize=10M（1.8之后版本）**：设置方法区的内存大小
- **-XX:MaxMetaspaceSize=10M（1.8之后版本）**：设置方法区内存的最大值
- **-XX:MaxDirectMemorySize=10M**：设置本机直接内存大小，不指定默认与Java堆最大值一样。

## 判断一个类是否为无用的类

1. 该类的实例都已经被回收了，也就是Java堆中不存在该类的任何实例。
2. 加载该类的ClassLoader已经被回收。
3. 该类对应的java.lang.Class对象没有在任何地方被引用，无法在任何地方通过反射访问该类的方法。

##  垃圾收集器

1. **Serial收集器**：**新生代采用复制算法，老年代采用标记整理算法。**单线程收集器、进行垃圾收集的时候必须暂停其他所有的工作线程，是虚拟机运行在**Client模式**下的**默认新生代收集器**。优于其他收集器的地方在于：简单而高效（与其他收集器的单线程相比）。
2. **ParNew收集器**：**新生代采用复制算法，老年代采用标记整理算法。ParNew收集器**其实就是**Serial收集器**的多线程版本，除了使用多条线程进行垃圾收集之外，其余行为包括Serial收集器可用的所有控制参数、收集算法、Stop The World、对象分配规则、回收策略等都和Serial收集器完全一样。是许多运行在**Server**模式下的虚拟机的首选的新生代收集器，除了**Serial收集器**，目前只有它能和**CMS收集器**配合工作。它是使用**-XX:+UseConcMarkSweepGC**选项后的默认新生代收集器，也可以使用**-XX:+UseParNewGC**选项来强制指定它，可以使用**-XX:ParallelGCThreads**参数来限制垃圾收集器的线程数。
3. **Parallel Scavenge收集器**：**新生代收集器，使用复制算法，是并行的多线程收集器**。目的是达到一个可控制的吞吐量，吞吐量=运行用户代码时间/（运行用户代码时间+垃圾收集时间），提供了两个参数：控制最大垃圾收集停顿时间的**-XX:MaxGCPauseMillis**参数以及直接设置吞吐量大小的**-XX:GCTimeRatio**参数。该收集器还有一个参数**-XX:+UseAdaptiveSizePolicy**，这是一个开关参数，当这个参数打开的时候，就不需要手工指定**新生代的大小（-Xmn)**、**Eden**与**Survivor**区的比例（**-XX:SurvivorRatio**)、晋升老年代对象大小（**-XX:PretenureSizeThreshold**)等细节参数了，虚拟机会根据当前系统运行情况收集性能监控信息动态调整。这种调节方式称为**GC**自适应的调节策略（**GC Ergonomics**）
4. **Serial Old收集器**：是**Serial收集器**的**老年代版本**，同样是**单线程**收集器，使用**标记-整理算法**，这个收集器的主要意义也是在于给**Client模式**下的虚拟机使用。如果在**Server模式**下，那么它主要还有两大用途：一种用途是在**JDK1.5**以及之前的版本中与**Parallel Scavenge**收集器搭配使用，另一种用途就是作为**CMS收集器**的后备预案，在并发收集发生**Concurrent Mode Failure**时使用。
5. **Parallel Old收集器**：是**Parallel Scavenge收集器**的**老年代版本**，使用**多线程**和**标记-整理算法**。**JDK1.6**开始提供。
6. **CMS收集器**：**CMS收集器**是一种以**获取最短回收停顿时间为目标的收集器**。**老年代收集器**，基于**标记-清除算法**，运作过程主要分为4个步骤：**初始标记（CMS initial mark）、并发标记（CMS concurrent mark）、重新标记（CMS remark）、并发清除（CMS concurrent sweep）**，其中**初始标记、重新标记**这两个步骤仍然需要**Stop The World**。**CMS**默认启动的回收线程数是**(CPU数量+3)/4**。在**JDK1.5**的默认设置下，**CMS**在老年代使用了**68%**的空间就会被激活，可以通过参数**-XX:CMSInitiatingOccupancyFraction**的值来提高触发百分比。在**JDK1.6**当中，启动阈值已经提升至**92%**。**CMS**提供了一个**-XX:+UseCMSCompactAtFullCollection**开关参数(默认就是开启的)，用于在**CMS收集器**顶不住要进行**FullGC**时开启内存碎片的合并整理过程。还提供了参数**-XX:CMSFullGCsBeforeCompaction**用于设置执行多少次不压缩的**Full GC**后，跟着来一次带压缩的（默认值为0，表示每次进入**Full GC**时都进行碎片整理）。

## G1收集器

### G1收集器的特点

- **并发与并行**：通过利用多CPU来缩短**Stop-The-World**停顿的时间
- **分代收集**：与其他收集器一样，分代概念在G1中仍然得以保留。G1收集器不需要与其他收集器配合就能独立管理整个GC堆。
- **空间整合**：G1从整体上来看是基于**标记-整理**算法实现的收集器，从局部（两个**Region**之间）上来看是基于**复制**算法实现的。
- **可预测的停顿**：降低停顿时间是G1和CMS共同的关注点，但G1除了追求停顿时间外，还建立可预测的停顿时间模型，能让使用者明确指定在一个长度为**M**毫秒的时间片段内，消耗在垃圾收集上的时间不得超过**N**毫秒，这几乎已经是实时Java（**RTSJ**）的垃圾收集器的特征了。

### G1收集器的内存分配

**G1收集器**将整个**Java**堆划分为多个大小相等的独立区域（**Region**）,虽然还保留有新生代和老年代的概念，但新生代和老年代不再是物理隔离的了，它们都是一部分**Region**（不需要连续）的集合。

### 如何避免全区域的垃圾收集

​	在**G1收集器**当中，**Region**之间的对象引用以及其他收集器中的新生代与老年代之间的对象引用，虚拟机都是使用**Remembered Set**来避免全堆扫描的。**G1**中的每个**Region**都有一个与之对应的**Remembered Set**，虚拟机发现程序在对**Reference**类型的数据进行写操作时，会产生一个**Write Barrier**暂时中断写操作，检查**Reference**引用的对象是否处于不同的**Region**之中（在分代的例子中就是检查是否老年代中的对象引用了新生代中的对象），如果是，便通过**CardTable**把相关的引用信息记录到被引用对象所属的**Region**的**Remembered Set**之中。当进行内存回收时，在**GC**根节点的枚举范围中加入**Remembered Set**即可保证不对全堆扫描也不会有遗漏。

### G1收集器的运行步骤

- **初始标记**：仅仅只是标记一下**GC Roots**能直接关联到的对象，并且修改**TAMS**（**Next Top at Mark Start**）的值，这个阶段停顿用户线程。
- **并发标记**：从**GC Roots**开始对堆中对象进行可达性分析，找出存活对象。可与用户线程并发执行。
- **最终标记**：这个阶段是为了修正在并发标记期间因用户程序继续运作而导致标记产生变动的那一部分标记记录，虚拟机将对象变化记录记录在线程的**Remembered Set Logs**里面，最终标记阶段需要把**Remembered Set Logs**的数据合并到**Remembered Set**中，这个阶段需要停顿线程，但是可并行执行。
- **筛选回收**：首先对各个**Region**的回收价值和成本进行排序，根据用户所期望的**GC**停顿时间来制定回收计划。停顿用户线程。



## 类加载的时机

### 类的生命周期

类的生命周期包括：**加载、验证、准备、解析、初始化、使用、卸载**7个阶段，其中**验证、准备、解析**三个阶段统称为**连接**。

什么时候需要开始类加载过程的第一个阶段：加载？Java虚拟机规范中并没有进行强制约束，但对于初始化阶段，虚拟机规范严格规定了有且只有5种情况必须立即对类进行**初始化**（而**加载、验证、准备**自然需要在此之前开始）

1. 遇到**new、getstatic、putstatic或invokestatic**这4条字节码指令时，如果类没有进行过初始化，则需要先触发初始化。生成这4条指令的最常见的Java代码场景是：使用**new**关键字实例化对象的时候、读取或设置一个类的静态字段（被**final**修饰、已在编译器把结果放入常量池的静态字段除外）的时候，以及调用一个类的静态方法的时候。
2. 使用**java.lang.reflect**包的方法对类进行放射调用的时候，如果类没有进行过初始化，则需要先触发其初始化。
3. 当初始化一个类的时候，如果发现其父类还没有进行过初始化，则需要先触发其父类的初始化。
4. 当虚拟机启动时，用户需要指定一个要执行的主类（包括**main**方法的那个类），虚拟机会先初始化这个主类。
5. 当使用**JDK1.7**的动态语言支持时，如果一个**java.lang.invoke.MethodHandle**实例最后的解析结果**REF_getStatic、REF_putStatic、REF_invokeStatic**的方法句柄，并且这个方法句柄所对应的类没有进行过初始化，则需要先触发其初始化。

对于这五种会触发类进行初始化的场景，虚拟机规范中使用了一个很强烈的限定语：**有且只有**，这五种场景中的行为称为对一个类进行**主动引用**。除此之外，所有引用类的方式都不会触发初始化，称为**被动引用**。

总结：

- **通过子类引用父类的静态字段，不会导致子类初始化。**
- **常量在编译阶段会存入调用类的常量池中，本质上并没有直接引用到定义常量的类，因此不会触发定义常量的类的初始化。（常量传播优化）**

### 类加载的过程

#### 加载

在加载阶段，虚拟机需要完成一下3件事情：

1. **通过一个类的全限定名来获取定义该类的二进制字节流。**
2. **将这个字节流所代表的静态存储结构转化为方法区的运行时数据结构。**
3. **在内存中生成一个代表这个类的java.lang.Class对象，作为方法区这个类的各种数据的访问入口。**

**数组类本身不通过类加载器加载，它是由Java虚拟机直接创建的。**加载阶段完成后，虚拟机外部的**二进制字节流**就按照虚拟机所需的格式存储在**方法区**中，然后在**内存**中实例化一个**java.lang.Class**类的对象（**对于HotSpot虚拟机而言，Class对象比较特殊，它虽然是对象，但是存放在方法区里面**），这个对象将作为程序访问**方法区**中的这些**类型数据**的外部接口。

#### 验证

##### 文件格式验证

##### 元数据验证

##### 字节码验证

##### 符号引用验证

#### 准备

> **准备阶段是正式为类变量分配内存并设置类变量初始值的阶段，这些变量所使用的内存都将在方法区中进行分配。类变量有两次赋初始值的过程：一次在准备阶段，赋予系统初始值；另一次在初始化阶段，赋予程序员定义的初始值。**

#### 初始化

类初始化阶段是类加载过程的最后一步，初始化阶段根据程序员通过程序制定的主观计划区初始化类变量和其他资源，或者可以从另外一个角度来表达：**初始化阶段是执行类构造器<clinit>()方法的过程**。

> <clinit>()方法是由 编译器自动收集类中的所有类变量的赋值动作和静态语句块（static{}块）中的语句合并产生的，编译器收集的顺序是由语句在源文件中出现的顺序所决定的，静态语句块中只能访问到定义在静态语句块之前的变量，定义在它之后的变量，在前面的静态语句块可以复制，但是不能访问。

示例：

```java
package example;

public class Test {
    static {
        i =0;
        System.out.println(i); // Illegal forward reference
    }
    static int i =1;
}
```

## 类加载器

对于任意一个类，都需要由加载它的类加载器和这个类本身一同确立其在java虚拟机中的唯一性，每一个类加载器，都拥有一个独立的类名称空间。这句话可以表达得更通俗一些：比较两个类是否**相等**，只有在这两个类是由同一个类加载器加载的前提下才有意义，否则，即使这两个类来源于同一个Class文件，被同一个虚拟机加载，只要加载他们的类加载器不同，那这两个类就必定不相等。

### 类加载器种类

从虚拟机的角度来讲，只存在两种不同的类加载器：一种是**启动类加载器（Bootstrap ClassLoader）**，这个类加载器使用C++实现，是虚拟机自身的一部分。另一种就是所有其他的类加载器，这些类加载都有Java语言实现，独立于虚拟机外部，并且都继承自抽象类**java.lang.ClassLoader**。

1. **启动类加载器（Bootstrap ClassLoader）**：这个类加载器负责将存放在**<JAVA_HOME>\lib**目录中的，或者被**-Xbootclasspath**参数所指定的路径中，并且是虚拟机识别的类库加载到虚拟机内存中。
2. **扩展类加载器（Extension ClassLoader）**：这个类加载器由**sun.misc.Launcher$ExtClassLoader**实现，它负责加载**<JAVA_HOME>\lib\ext**目录中的，或者被**java.ext.dirs**系统变量所指定的路径中的所有类库，开发者可以直接使用扩展类加载器。
3. **应用程序类加载器（Application ClassLoader）**：这个类加载器由**sun.misc.Launcher$AppClassLoader**实现。由于这个类加载器是**ClassLoader**中的**getSystemClassLoader()**方法的返回值，所以一般也称它为**系统类加载器**。它负责加载**用户类路径（ClassPath）**上指定的类库，开发者可以直接使用这个类加载器，如果应用程序中没有自定义过自己的类加载器，一般情况下这个就是程序中默认的类加载器。





<img src="C:\Users\Administrator\Desktop\study-notes\笔记\images\image-20200106173649138.png" alt="image-20200106173649138" style="zoom: 80%;" />

**双亲委派模型的工作过程**是：如果一个类加载器收到了类加载的请求，它首先不会自己去尝试加载这个类，而是把这个请求委派给父类加载器去完成，每一个层次的类加载器都是如此，因此所有的加载请求最终都应该传送到顶层的启动类加载器中，只有当父类加载器反馈自己无法完成这个加载请求（它的搜索范围中没有找到所需的类）时，子类加载器才会尝试自己去加载。

## 虚拟机字节码执行引擎

###  运行时栈帧结构

**栈帧**是用于支持虚拟机进行方法调用和方法执行的数据结构，栈帧存储了方法的**局部变量表、操作数栈、动态链接和方法返回地址**等信息。每一个方法从调用开始至执行完成的过程，都对应着一个栈帧在虚拟机栈里面从入栈到出栈的过程。对于执行引擎来说，在活动线程中，只有位于栈顶的栈帧才是有效的，称为当前栈帧，与这个栈帧有关联的方法称为当前方法，执行引擎运行的所有字节码指令都只针对当前栈帧进行操作。

#### 局部变量表

​	用于存放方法参数和方法内部定义的局部变量，在Java程序编译为Class文件时，就在方法的**Code属性**的**max_local**数据项中确定了该方法所需要分配的局部变量表的最大容量。虚拟机通过**索引定位**的方式使用局部变量表，索引值的范围是**从0开始至局部变量表最大的Slot数量**，如果是32位数据类型，索引n就代表使用第n个Slot，64位则会同时使用n和n+1两个Slot。对于共同存放一个64位数据的两个Slot，不允许采用任何一个方式单独访问其中任何一个，如果遇到了则应该在类加载的校验阶段抛出异常。

​	方法执行时，如果执行的是实例方法（非static方法），那局部变量表中**第0位索引的Slot**默认是用于传递方法所属对象实例的引用，在方法中可以通过**this**来访问这个隐含的参数。

​	局部变量表中的**Slot**是可以重用的，如果当前**字节码PC计数器**的值已经超过了某个变量的作用域，那这个变量对应的Slot就可以交给其他变量使用。

​	类变量有两次赋初始值的过程，一次在准备阶段，赋予系统初始值；另外一次在初始化阶段，赋予程序员定义的初始值。即使在初始化阶段程序员没有为类变量赋值，类变量仍然具有一个确定的初始值。但局部变量如果定义了但没有赋初始值则不能使用。

#### 操作数栈

​	操作数栈也常称为操作栈，操作数栈的最大深度在编译时写入到**Code属性**的**max_stacks**数据项中。操作数栈的每一个元素可以是任意的Java数据类型，包括long和double。32位数据类型所占的栈容量为1，64位为2。

**Java虚拟机的解释执行引擎称为基于栈的执行引擎，其中所指的栈就是操作数栈**

#### 动态连接

​	在一个class文件中，一个方法要调用其他方法，需要将这些方法的**符号引用**转化为其在内存地址中的**直接引用**，而**符号引用存在于方法区中的运行时常量池**。每一个栈帧都包含一个指向**运行时常量池**中该栈帧所属方法的引用（**符号引用**），持有这个引用是为了支持**方法调用过程中的动态连接**。字节码中的方法调用指令就以常量池中指向方法的符号引用作为参数。这些符号引用一部分会在类加载阶段或者第一次使用的时候就转化为直接引用，这种转化称为**静态解析**。另一部分将在每一次运行期间转化为直接引用，这部分称为**动态连接**。

#### 方法返回地址

​	当一个方法开始执行后，只有两种方式可以退出这个方法。第一种方式是执行引擎遇到任意一个方法返回的字节码指令，这种方式称为**正常完成出口**。另一种退出方式是，在方法执行过程中遇到了异常，并且这个异常没有在方法体内得到处理，这种方式称为**异常完成出口**。

​	方法正常退出时，调用者的**PC计数器**的值可以作为返回地址，而方法异常退出时，返回地址时要通过异常处理表来确定的。方法退出的过程实际上就等同于把当前栈帧出栈，因此退出时可能执行的操作有：恢复上层方法的局部变量表和操作数栈，把返回值（如果有的话）压入调用者栈帧的操作数栈中，调整PC计数器的值以指向方法调用指令后面的一条指令等。

### 方法调用

#### 解析

​	所有方法调用中的目标方法在Class文件里面就是一个**常量池**的**符号引用**，在类加载的解析阶段，会将其中一部分符号引用转化为直接引用，调用目标在程序代码写好、编译器进行编译时就必须确定下来。这类方法的调用称为**解析**。

​	在Java语言中符合**编译器可知，运行期不可变**的这个要求的方法，主要包括**静态方法**和**私有方法**两大类。

> **Java虚拟机里面提供了5条方法调用字节码指令**：
>
> 1. **invokestatic**：调用静态方法
>
> 2. **invokespecial**：调用实例构造器<init>方法、私有方法和父类方法
>
> 3. **invokevirtual**：调用所有的虚方法
>
> 4. **invokeinterface**：调用接口方法，会在运行时再确定一个实现该接口的对象。
>
> 5. **invokedynamic**：先在运行时动态解析出调用点限定符所引用的方法，然后再执行该方法，在此之前的4条调用，分派逻辑是固化在Java虚拟机内部的，而**invokedynamic**指令的分派逻辑是由用户所设定的引导方法决定的。
>
>    ##### 虚方法与非虚方法
>
>    **只要能被invokestatic和invokespecial指令调用的方法，都可以在解析阶段中确定唯一的调用版本，符合这个条件的有静态方法、私有方法、实例构造器、父类方法4类**。在类加载的时候就会把符号引用解析为该方法的直接引用。这些方法可以称为**非虚方法**，与之相反，其他方法称为**虚方法**（除去**final**方法，**final**方法虽然是使用**invokevirtual**调用的，但是由于它无法被覆盖，没有其他版本，所以无须进行**多态选择**，**final方法是非虚方法**）
>
>    **解析调用一定是个静态过程，在编译器就完全确定，在类装载的解析阶段就会把涉及到的符号引用全部转变为可确定的直接引用。**

#### 分派

##### 静态分派

示例：

```java
package example;

public class StaticDispatch {
    static class Human {

    }

    static class Man extends Human {

    }

    static class Woman extends Human {

    }


    public void sayHello(Human human) {
        System.out.println("human say hello!");
    }
    public void sayHello(Woman human) {
        System.out.println("human say hello!");
    }
    public void sayHello(Man human) {
        System.out.println("human say hello!");
    }

    public static void main(String[] args) {
        Human woman = new Woman();
        Human man =new Man();

        StaticDispatch sd = new StaticDispatch();
        sd.sayHello(woman);
        sd.sayHello(man);
    }
}

```

​	上面代码中，**Human**称为变量的**静态类型**，**Man**则称为变量的**实际类型**，**静态类型**的变化仅仅在使用时发生，变量本身的**静态类型**不会被改变，并且最终的**静态类型**是在**编译期**可知的，**实际类型**变化的结果在运行期才可确定。**虚拟机（准确来说是编译器）在重载时是通过参数的静态类型而不是实际类型作为判定依据的**。

​	所有依赖静态类型来定位方法执行版本的分派动作称为**静态分派**。**静态分派**的典型应用是方法重载。**静态分派**发生在编译阶段。

##### 动态分派

**动态分派**的重要体现是**重写**。

示例：

```java
package example;

public class DynamicDispatch {


    static abstract class Human {
        public abstract void sayHello();
    }

    static class Man extends Human {

        @Override
        public void sayHello() {
            System.out.println("man say hello");
        }
    }

    static class Woman extends Human {

        @Override
        public void sayHello() {
            System.out.println("woman say hello");
        }
    }




    public static void main(String[] args) {
        Human man = new Man();
        Human woman = new Woman();
        man.sayHello();
        woman.sayHello();
        man = new Woman();
        man.sayHello();
    }
}
```

**invokevirtual**指令的**多态查找过程**：

1. 找到操作数栈顶的第一个元素所指向的对象的实际类型，记作C。
2. 如果在类型C中找到与常量中描述符和简单名称都相符的方法，则进行访问权限校验，如果通过则返回这个方法的直接引用，查找过程结束；如果不通过，则返回**java.lang.IllegalAccessError异常。
3. 否则，按照继承关系从下往上依次对C的各个父类进行第2步的搜索和验证过程。
4. 如果始终没有找到合适的方法，则抛出**java.lang.AbstractMethodError异常。

由于**invokevirtual**指令执行的第一步就是在运行期确定接收者的**时机类型**，所以两次调用中的**invokevirtual**指令把常量池中的**符号引用**解析到了不同的**直接引用**上，这个过程就是Java语言中**重写**的本质。我们把这种在运行期**根据实际类型确定方法执行版本的分派过程称为动态分派**。

由于**动态分派**是非常频繁的动作，所以最常用的稳定优化的方式是为类在方法区中建立一个**虚方法表**（Virtual Method Table，简称itable），使用虚方法表索引来代替元数据查找以提高性能。方法表一般在类加载的连接阶段进行初始化，准备了类的变量初始值后，虚拟机会把该类的方法表也初始化完毕。

​	

### 基于栈的字节码解释执行引擎

#### 解释执行

​	Java语言中**javac编译器**完成了程序代码经过**词法分析、语句分析和抽象语法树，再遍历语法树生成线性的字节码指令流的过程**

#### 基于栈的指令集和基于寄存器的指令集

​	Java编译器输出的指令流，基本上是一种**基于栈的指令集架构**。使用栈架构的指令集，用户程序不会直接使用这些寄存器，就可以由虚拟机实现来自行决定把一些访问最频繁额数据（**程序计数器、栈顶缓存等**）放到寄存器中以获取尽量好的性能。

​	栈实现在内存之中，频繁的栈访问也就意味着频繁的内存访问，虚拟机可以采用**栈顶缓存**的手段，把最常用的操作映射到寄存器之中避免**直接内存访问**。

## 早期（编译期）优化

### 解析与填充符号表

1. **词法、语法分析**
2. **填充符号表**

### 语义分析与字节码生成

1. **标注检查**
2. **数据及控制流分析**
3. **解语法糖**
4. **字节码生成**

### Java语法糖的味道

#### switch开始支持String

```java
public class switchDemoString
{
    public switchDemoString()
    {
    }
    public static void main(String args[])
    {
        String str = "world";
        String s;
        switch((s = str).hashCode())
        {
        default:
            break;
        case 99162322:
            if(s.equals("hello"))
                System.out.println("hello");
            break;
        case 113318802:
            if(s.equals("world"))
                System.out.println("world");
            break;
        }
    }
}
```

​	**`swith`自身原本就支持基本类型。比如`int`、`char`等。对于`int`类型，直接进行数值的比较。对于`char`类型则是比较其`ascii`码。字符串的switch是通过`equals()`和`hashCode()`方法来实现的。**

#### 泛型与类型擦除

```java
Map map = new HashMap();  
map.put("name", "hollis");  
map.put("wechat", "Hollis");  
map.put("blog", "www.hollischuang.com"); 
```

​	**虚拟机中没有泛型，只有普通类和普通方法，所有泛型类的类型参数在编译时都会被擦除，泛型类并没有自己独有的`Class`类对象。比如并不存在`List.class`或是`List.class`，而只有`List.class`。**

​	**类型擦除的主要过程如下： 1.将所有的泛型参数用其最左边界（最顶级的父类型）类型替换。 2.移除所有的类型参数。**

#### 自动装箱与拆箱

```
public static void main(String args[])
{
    Integer i = Integer.valueOf(10);
    int n = i.intValue();
}
```

​	**装箱过程是通过调用包装器的valueOf方法实现的，而拆箱过程是通过调用包装器的 xxxValue方法实现的。**

#### 方法变长参数

```java
public static void main(String args[])
{
    print(new String[] {
        "Holis", "\u516C\u4F17\u53F7:Hollis", "\u535A\u5BA2\uFF1Awww.hollischuang.com", "QQ\uFF1A907607222"
    });
}

public static transient void print(String strs[])
{
    for(int i = 0; i < strs.length; i++)
        System.out.println(strs[i]);

}
```

​	**从反编译后代码可以看出，可变参数在被使用的时候，他首先会创建一个数组，数组的长度就是调用该方法是传递的实参的个数，然后再把参数值全部放到这个数组当中，然后再把这个数组作为参数传递到被调用的方法中。**

#### 枚举

```java
public final class example.Color extends java.lang.Enum<example.Color> {
  public static final example.Color RED;
  public static final example.Color GREEN;
  public static final example.Color BLUE;
  java.lang.String code;
  java.lang.String message;
  public static example.Color[] values();
  public static example.Color valueOf(java.lang.String);
  static {};
}
```

​	**当我们使用`enmu`来定义一个枚举类型的时候，编译器会自动帮我们创建一个`final`类型的类继承`Enum`类，所以枚举类型不能被继承，如果要为 enum 定义方法，那么必须在 enum 的最后一个实例尾部添加一个分号。此外，在 enum 中，必须先定义实例，不能将字段或方法定义在实例前面。否则，编译器会报错。**

#### 数值字面量

源代码：

```java
public class Test {
    public static void main(String... args) {
        int i = 10_000;
        System.out.println(i);
    }
}
```

编译后：

```java
public class Test
{
  public static void main(String[] args)
  {
    int i = 10000;
    System.out.println(i);
  }
}
```

​	**数值字面量，不管是整数还是浮点数，都允许在数字之间插入任意多个下划线。这些下划线不会对字面量的数值产生影响，目的就是方便阅读。**

#### for-each

源代码：

```java
public static void main(String... args) {
    String[] strs = {"Hollis", "公众号：Hollis", "博客：www.hollischuang.com"};
    for (String s : strs) {
        System.out.println(s);
    }
    List<String> strList = ImmutableList.of("Hollis", "公众号：Hollis", "博客：www.hollischuang.com");
    for (String s : strList) {
        System.out.println(s);
    }
}
```

反编译：

```java
public static transient void main(String args[])
{
    String strs[] = {
        "Hollis", "\u516C\u4F17\u53F7\uFF1AHollis", "\u535A\u5BA2\uFF1Awww.hollischuang.com"
    };
    String args1[] = strs;
    int i = args1.length;
    for(int j = 0; j < i; j++)
    {
        String s = args1[j];
        System.out.println(s);
    }

    List strList = ImmutableList.of("Hollis", "\u516C\u4F17\u53F7\uFF1AHollis", "\u535A\u5BA2\uFF1Awww.hollischuang.com");
    String s;
    for(Iterator iterator = strList.iterator(); iterator.hasNext(); System.out.println(s))
        s = (String)iterator.next();

}
```

**for-each的实现原理其实就是使用了普通的for循环和迭代器。**

#### try-with-resource

源代码：

```java
public static void main(String... args) {
    try (BufferedReader br = new BufferedReader(new FileReader("d:\\ hollischuang.xml"))) {
        String line;
        while ((line = br.readLine()) != null) {
            System.out.println(line);
        }
    } catch (IOException e) {
        // handle exception
    }
}
```

反编译：

```
public static transient void main(String args[])
    {
        BufferedReader br;
        Throwable throwable;
        br = new BufferedReader(new FileReader("d:\\ hollischuang.xml"));
        throwable = null;
        String line;
        try
        {
            while((line = br.readLine()) != null)
                System.out.println(line);
        }
        catch(Throwable throwable2)
        {
            throwable = throwable2;
            throw throwable2;
        }
        if(br != null)
            if(throwable != null)
                try
                {
                    br.close();
                }
                catch(Throwable throwable1)
                {
                    throwable.addSuppressed(throwable1);
                }
            else
                br.close();
            break MISSING_BLOCK_LABEL_113;
            Exception exception;
            exception;
            if(br != null)
                if(throwable != null)
                    try
                    {
                        br.close();
                    }
                    catch(Throwable throwable3)
                      {
                        throwable.addSuppressed(throwable3);
                    }
                else
                    br.close();
        throw exception;
        IOException ioexception;
        ioexception;
    }
}
```

