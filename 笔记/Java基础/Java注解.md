# Java注解

## 元注解

元注解简单来说就是定义其他注解的注解。主要有四个：

- **@Target**：表示该注解可以用于什么地方
- **@Retention**：表示在什么级别保存该注解信息
- **@Documented**：将此注解包含在javadoc中
- **@Inherited**：允许子类继承父类中的注解

## Spring常用组件

- **@Configuration**：把一个类作为一个IOC容器，它的某个方法头上如果注册了@Bean，就会作为这个Spring容器中的Bean。

- **@Lazy(true)**： 表示延迟初始化

- **@Service**：用于标注业务层组件、

- **@Controller**：用于标注控制层组件

- **@Repository**：用于标注数据访问组件，即DAO组件。

- **@Component**：泛指组件，当组件不好归类的时候，我们可以使用这个注解进行标注。

- **@Scope**：用于指定scope作用域的（用在类上）

- **@PostConstruct**：用于指定初始化方法（用在方法上）

- **@PreDestory**：用于指定销毁方法（用在方法上）

- **@DependsOn**：定义Bean初始化及销毁时的顺序

- **@Primary**：自动装配时当出现多个Bean候选者时，被注解为@Primary的Bean将作为首选者，否则将抛出异常

- **@Autowired**: 默认按类型装配，如果我们想使用按名称装配，可以结合@Qualifier注解一起使用。如下：

  @Autowired @Qualifier("personDaoBean") 存在多个实例配合使用

- **@Resource**：默认按名称装配，当找不到与名称匹配的bean才会按类型装配。