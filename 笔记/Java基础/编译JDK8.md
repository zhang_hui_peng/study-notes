**下载boot jdk**：必须比要编译的jdk低一个版本

**设置LANG**：

```shell
export LANG=C
```

**禁用相关环境变量**：

```shell
unset _JAVA_OPTIONS 
unset CLASSPATH
unset JAVA_HOME
```

**安装libX11-dev**:应该是libx11-dev，X为小写

**修改hotspot/make/linux/Makefile**：添加4%

**添加多个版本**（编译版本需要为**gcc-4.9**）：

```shell
update-alternatives --install /usr/bin/g++ g++ /usr/bin/x86_64-linux-gnu-g++-4.9 50
update-alternatives --install /usr/bin/gcc gcc /usr/bin/x86_64-linux-gnu-gcc-4.9 50
```

**修改/hotspot/make/linux/makefiles/gcc.make**：注释掉 ==WARNINGS_ARE_ERRORS = -Werror==

