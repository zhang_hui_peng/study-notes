# shell脚本笔记

## 基础知识

- **$@**代表所有参数，**$#**代表所有参数的个数

- 引用数组的时候前面加**!**表示取数组下标

- 变量严格区分大小写

- 在变量、等号和值之间不能出现空格

- 引用一个变量值时需要使用**$**，而引用变量来对其进行赋值时则不要使用**$**

- 有两种方法可以将命令输出赋给变量，反引号(**`**)和**$()**格式。

  ```bash
  #/bin/bash
  testing=`date`
  testing=$(date)
  ```

- **date +%y%m%d**是告诉**date**命令将日期显示为两位数的年月日的组合
- 输出重定向：使用 **>** 来将命令的输出发送到一个文件中，**command > outputfile**，如果文件已存在默认覆盖，追加数据使用 **>>**
- 输入重定向：使用 **<** 来将文件的内容重定向到命令，**command < inputfile**
- 将命令输出重定向到另一个命令当中，可以使用管道连接。**command | command**,第一个命令的输出会被立即送到第二个命令
- **$?**变量用来保存上个已执行命令的退出状态码，一个成功结束的命令的状态码是0，如果命令结束时有错误，状态码是正数值。可以使用**exit 状态码**来指定返回的退出状态码
- **if**可以使用方括号**[]**来替代测试条件，第一个括号之后和最后一个括号之前必须有一个空格