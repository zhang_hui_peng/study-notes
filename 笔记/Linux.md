# Linux

设置时区：**timedatectl set-timezone Asia/Shanghai**

压缩包切分：**split -b 4000M -d -a 1 cm-11.tar.gz cm-11.tar.gz.**

- -b 4000M 表示设置每个分割包的大小，单位还是可以k
- -d "参数指定生成的分割包后缀为数字的形式
- -a x来设定序列的长度(默认值是2)，这里设定序列的长度为1

分割后的压缩包解压：**cat cm-11.tar.gz.* | tar -zxv**

